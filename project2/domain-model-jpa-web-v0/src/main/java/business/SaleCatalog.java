package business;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * A catalog for sales
 * 
 * @author fmartins
 * @version 1.1 (17/04/2015)
 *
 */
public class SaleCatalog {

	/**
	 * Entity manager factory for accessing the persistence service 
	 */
	private EntityManagerFactory emf;
	
	/**
	 * Constructs a sale's catalog giving a entity manager factory
	 */
	public SaleCatalog(EntityManagerFactory emf) {
		this.emf = emf;
	}

	/**
	 * Creates a new sale and adds it to the repository
	 * 
	 * @param customer The customer the sales belongs to
	 * @return The newly created sale
	 */
	public Sale newSale (Customer cliente) throws ApplicationException {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Sale sale = new Sale(new Date(), cliente);
			em.persist(sale);
			em.getTransaction().commit();
			return sale;
 		} catch (Exception e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new ApplicationException("Error adding sale", e);
		} finally {
			em.close();
		}
	}

	// @pre venda.isEmAberto()
	// @pre produto.qty() >= qty
	/**
	 * @param sale
	 * @param product
	 * @param qty
	 * @throws ApplicationException
	 */
	public void addProductToSale (Sale sale, Product product, double qty) 
				throws ApplicationException {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(product);
			em.merge(sale);
			sale.addProductToSale(product, qty);
			em.getTransaction().commit();
 		} catch (Exception e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new ApplicationException("Error adding product to sale", e);
		} finally {
			em.close();
		}
	}

}
