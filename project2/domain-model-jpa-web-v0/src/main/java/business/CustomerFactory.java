package business;

public interface CustomerFactory {

	Object createCustomer(int vat, String designation, int phoneNumber,
			Discount discountType);
}
