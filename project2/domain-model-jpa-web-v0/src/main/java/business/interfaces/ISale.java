package business.interfaces;

import business.ApplicationException;
import business.Customer;


public interface ISale {

	public double total();

	public double eligibleDiscountTotal();
	
	public Customer getCustomer();
	
	public double discount () throws ApplicationException;
}