package business.interfaces;


public interface IDiscount {

	String getDescription();

	int getId();

}