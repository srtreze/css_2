package business;

public class CustomerFactoryImpl implements CustomerFactory {

	@Override
	public Object createCustomer(int vat, String designation, int phoneNumber,
			Discount discountType) {
		return new Customer(vat, designation, phoneNumber, discountType);
	}

}
