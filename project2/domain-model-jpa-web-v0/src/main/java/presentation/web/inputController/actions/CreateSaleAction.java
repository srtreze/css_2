package presentation.web.inputController.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.web.model.NewSaleModel;
import business.ApplicationException;
import business.SaleSys;
import business.handers.ProcessSaleHandler;

/**
 * Handles the criar cliente event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response. 
 * 
 * @author fmartins
 *
 */
public class CreateSaleAction extends Action {

	@Override
	public void process(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		SaleSys app = (SaleSys) request.getServletContext().getAttribute("app");
		ProcessSaleHandler processSaleHandler = app.getProcessSaleHandler();

		NewSaleModel model = createModel(request);
		request.setAttribute("model", model);
		
		if (isInputValid(model)) {
			try {
				model.setCurrentSale(processSaleHandler.newSale(intValue(model.getVATNumber()))); 
				model.clearFields();
				model.addMessage("Venda criada com sucesso.");
			} catch (ApplicationException e) {
				model.addMessage("Erro ao criar venda: " + e.getMessage());
			}
		} else
			model.addMessage("Erro ao validar venda");
		
		request.getRequestDispatcher("/processSale/newSale.jsp").forward(request, response);
	}

	/**
	 * Validate the input request
	 * 
	 * @param nch The model object to be field.
	 * @return True if the fields are inputed correctly
	 */	
	private boolean isInputValid(NewSaleModel nvh) {		
		// check if VATNumber is filled and a valid number
		boolean result = isFilled(nvh, nvh.getVATNumber(), "É obrigatório preencher o número de pessoa colectiva")
				 			&& isInt(nvh, nvh.getVATNumber(), "Número de pessoal colectiva contém caracteres não numéricos");
		
		return result;
	}	

	/**
	 * Create the sale model object and fills it with data from the request
	 * 
	 * @param request The current HTTP request
	 * @param app The app object
	 * @return The brand new sale model object
	 */
	private NewSaleModel createModel(HttpServletRequest request) {
		// Create the object model
		NewSaleModel model = new NewSaleModel();

		// fill it with data from the request
		model.setVATNumber(request.getParameter("npc"));
		
		return model;
	}	

}
