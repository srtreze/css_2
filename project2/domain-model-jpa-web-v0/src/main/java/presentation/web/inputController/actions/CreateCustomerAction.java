package presentation.web.inputController.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.web.model.NewCustomerModel;
import business.ApplicationException;
import business.SaleSys;
import business.handers.AddCustomerHandler;

/**
 * Handles the criar cliente event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response. 
 * 
 * @author fmartins
 *
 */
public class CreateCustomerAction extends Action {
	
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		SaleSys app = (SaleSys) request.getServletContext().getAttribute("app");
		AddCustomerHandler addCustomerHandler = app.getAddCustomerHandler();

		NewCustomerModel model = createModel(request, app);
		request.setAttribute("model", model);
		
		if (isInputValid(model)) {
			try {
				addCustomerHandler.addCustomer(intValue(model.getVATNumber()), 
						model.getDesignation(), intValue(model.getPhoneNumber()), intValue(model.getDiscountType()));
				model.clearFields();
				model.addMessage("Cliente criado com sucesso.");
			} catch (ApplicationException e) {
				model.addMessage("Erro ao criar cliente: " + e.getMessage());
			}
		} else
			model.addMessage("Erro ao validar cliente");
		
		request.getRequestDispatcher("/addCustomer/newCustomer.jsp").forward(request, response);
	}

	
	/**
	 * Validate the input request
	 * 
	 * @param nch The model object to be field.
	 * @return True if the fields are inputed correctly
	 */
	private boolean isInputValid(NewCustomerModel nch) {

		// check if designation is filled
		boolean result = isFilled(nch, nch.getDesignation(), "É obrigatório preencher a Designação.");
		
		// check if VATNumber is filled and a valid number
		result &= isFilled(nch, nch.getVATNumber(), "É obrigatório preencher o número de pessoa colectiva")
				 			&& isInt(nch, nch.getVATNumber(), "Número de pessoal colectiva contém caracteres não numéricos");
		
		// check in case phoneNumber is filled if it contains a valid number
		if (!nch.getPhoneNumber().equals(""))
			result &= isInt(nch, nch.getPhoneNumber(), "Número de telefone contém caracteres não numéricos");

		// check if discount type is filled and a valid number
		result &= isFilled(nch, nch.getDiscountType(), "É obrigatório preencher o Tipo de desconto") 
					&& isInt(nch, nch.getDiscountType(), "Tipo de desconto contém caracteres não numéricos");

		return result;
	}


	/**
	 * Create the customer model object and fills it with data from the request
	 * 
	 * @param request The current HTTP request
	 * @param app The app object
	 * @return The brand new customer model object
	 */
	private NewCustomerModel createModel(HttpServletRequest request, SaleSys app) {
		// Create the object model
		NewCustomerModel model = new NewCustomerModel();
		model.setAddCustomerHandler(app.getAddCustomerHandler());

		// fill it with data from the request
		model.setDesignation(request.getParameter("designacao"));
		model.setVATNumber(request.getParameter("npc"));
		model.setPhoneNumber(request.getParameter("telefone"));
		model.setDiscountType(request.getParameter("desconto"));
		
		return model;
	}	
}
