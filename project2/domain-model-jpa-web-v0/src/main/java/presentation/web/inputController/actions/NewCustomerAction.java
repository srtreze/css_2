package presentation.web.inputController.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import presentation.web.model.NewCustomerModel;
import business.SaleSys;


/**
 * Handles the novo cliente event
 * 
 * @author fmartins
 *
 */
public class NewCustomerAction extends Action {
	
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		SaleSys app = (SaleSys) request.getServletContext().getAttribute("app");

		NewCustomerModel model = new NewCustomerModel();
		model.setAddCustomerHandler(app.getAddCustomerHandler());
		request.setAttribute("model", model);
		request.getRequestDispatcher("/addCustomer/newCustomer.jsp").forward(request, response);
	}

}
