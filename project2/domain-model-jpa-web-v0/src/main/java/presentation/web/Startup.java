package presentation.web;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import business.SaleSys;

/**
 * Application Lifecycle Listener implementation class Leasing
 *
 */
@WebListener
public class Startup implements ServletContextListener {

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event)  { 
    	EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("domain-model-jpa-web");
    	SaleSys app = new SaleSys(emf);
        event.getServletContext().setAttribute("emf", emf);
        event.getServletContext().setAttribute("app", app);
    }

    /**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent event)  {
        EntityManagerFactory emf =
                (EntityManagerFactory) event.getServletContext().getAttribute("emf");
    	emf.close();
    }

	
}
