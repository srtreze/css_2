package presentation.web.model;

import business.interfaces.ISale;

/**
 * Model class to assist in the response of a new customer action.
 * This class is the response information expert.
 * 
 * @author fmartins
 *
 */
public class NewSaleModel extends Model {

	private String vatNumber;
	private ISale sale;
		
	public String getVATNumber() {
		return vatNumber;
	}
	
	public void setVATNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
		
	public void clearFields() {
		vatNumber = "";
	}

	public void setCurrentSale(ISale sale) {
		this.sale = sale;		
	}
	
	public String getDesignation() {
		return sale == null ? "" : sale.getCustomer().getDesignation();
	}
}
