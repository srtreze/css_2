package presentation.cli.controllers;

import business.ApplicationException;
import business.handers.ProcessSaleHandler;
import business.interfaces.ISale;

/**
 * Handles the sales' use case. 
 *	
 * @author fmartins
 *
 */
public class ProcessSaleService {

	private ProcessSaleHandler saleHandler;
	private ISale currentSale;

	public ProcessSaleService(ProcessSaleHandler saleHandler) {
		this.saleHandler = saleHandler;
	}
	
	public void newSale (int vatNumber) throws ApplicationException {
		currentSale = saleHandler.newSale(vatNumber);
	}

	public void addProductToSale (int productCode, double qty) throws ApplicationException {
		saleHandler.addProductToSale(productCode, qty);
	}
	
	public double getSaleDiscount () throws ApplicationException {
		return currentSale.discount();
	}
}
