package client;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import presentation.cli.controllers.AddCustomerService;
import presentation.cli.controllers.ProcessSaleService;
import business.ApplicationException;
import business.SaleSys;

/**
 * The big bang class.
 *	
 * @author fmartins
 */
public class SimpleClient {

	/**
	 * Fire in the hole!!
	 * 
	 * @param args Command line parameters
	 */
	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("domain-model-jpa");
			
			// Creates the business application 
			SaleSys app = new SaleSys (emf);

			// The application services
			AddCustomerService cs = new AddCustomerService (app.getAddCustomerHandler());
			ProcessSaleService ss = new ProcessSaleService(app.getProcessSaleHandler());

			// the interaction
			try {		
				// adds a customer.
				cs.addCustomer(168027852, "Customer 1", 217500255, 2);

				// starts a new sale
				ss.newSale(168027852);

				// adds two products to the sale
				ss.addProductToSale(123, 6);
				ss.addProductToSale(124, 5);
				ss.addProductToSale(123, 4);

				// gets the discount amount
				System.out.println(ss.getSaleDiscount());
			} catch (ApplicationException e) {
				System.out.println("Error: " + e.getMessage());
				// for debugging purposes only. Typically, in the application
				// this information can be associated with a "details" button when
				// the error message is displayed.
				if (e.getCause() != null) 
					System.out.println("Cause: ");
				e.printStackTrace();
			}	
		} catch(Exception e){
			System.out.println(e.getMessage());
		} finally{
			emf.close();
		}
	}
}
