package business;

import javax.persistence.*;

/**
 * A provider
 *
 * Created by Telma on 07/12/16.
 */


@Entity
@NamedQueries({
        @NamedQuery(name=Provider.FIND_BY_CODE, query="SELECT p FROM Provider p WHERE p.code = :" +
                Provider.CODE),
        @NamedQuery(name=Provider.ALL_PROVIDERS, query="SELECT p FROM Provider p")
})
public class Provider {

    // Named query name constants
    public static final String FIND_BY_CODE = "Provider.findByCode";
    public static final String CODE = "code";
    public static final String ALL_PROVIDERS = "Provider.findAllProviders";


    /**
     * Order primary key. Needed by JPA. Notice that it is not part of the
     * original domain model.
     */
    @Id
    @GeneratedValue
    private int id;


    /**
     * provider's code number
     */
    @Column(nullable = false, unique = true) private int code;


    /**
     * provider's name. In case of a company, the represents its commercial denomination
     */
    @Column(nullable = false)
    private String name;


    /**
     * provider's contact number
     */
    @SuppressWarnings("unused")
    private int phoneNumber;


    // 1. constructor

    /**
     * Constructor needed by JPA.
     */
    protected Provider() {
    }

    /**
     * Creates a new provider given its code number, its name, phone contact
     *
     * @param code The customer's VAT number
     * @param name The customer's designation
     */
    public Provider(int code, String name, int phoneNumber) {
        this.code = code;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }


    // 2. getters and setters

    public int getProviderId() {
        return this.id;
    }

    public int getProviderCode() {
        return this.code;
    }

    public void setProviderCode(int newcode) {
        this.code = newcode;
    }

    public String getProviderName() {
        return this.name;
    }

    public void setProviderName(String newname) {
        this.name = newname;
    }

    public int getProviderPhoneNumber() {
        return this.phoneNumber;
    }

    public void setProviderPhoneNumber(int newPhoneNumber) {
        this.phoneNumber = newPhoneNumber;
    }
}
