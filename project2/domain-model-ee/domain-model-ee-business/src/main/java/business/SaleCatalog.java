package business;

import facade.exceptions.ApplicationException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Telma on 07/12/16.
 */
@Stateless
public class SaleCatalog {


    /**
     * Entity manager for accessing the persistence service
     */
    @PersistenceContext
    private EntityManager em;


    /**
     * Finds an sale given its customer.
     *
     * @param customer
     * @return The Sale object corresponding to the sale with the customer.
     * @throws ApplicationException When the Sale with the customer is not found.
     */
    public Sale getSaleByCustomer(Customer customer) throws ApplicationException {
        TypedQuery<Sale> query = em.createNamedQuery(Sale.FIND_BY_CUSTOMER, Sale.class);
        query.setParameter(Sale.CUSTOMER, customer);
        try {
            return query.getSingleResult();
        } catch (PersistenceException e) {
            throw new ApplicationException("Order not found.");
        }
    }


    /**
     * Adds a new sale
     *
     * @param date     The date
     * @param customer The customer
     * @throws ApplicationException When the sale is already in the repository
     */
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public Sale addSale(Date date, Customer customer)
            throws ApplicationException {
        Sale sale = new Sale(date, customer);
        em.persist(sale);
        return sale;
    }


    public Sale getSaleById(int id) throws ApplicationException {
        Sale s = em.find(Sale.class, id);
        if (s == null)
            throw new ApplicationException("Sale with id " + id + " not found");
        else
            return s;
    }

    public Iterable<Sale> getSales() {
        TypedQuery<Sale> query = em.createNamedQuery(Sale.FIND_ALL_SALES, Sale.class);
        return new LinkedList<>(query.getResultList());
    }

//    public void deleteSale(int id) throws ApplicationException {
//        Sale s = em.find(Sale.class, id);
//        if (s == null)
//            throw new ApplicationException("Sale with id " + id + " not found");
//        em.remove(s);
//    }

    /**
     * @param sale
     * @param product
     * @param qty
     * @throws ApplicationException
     */
    public Sale addProductToSale(Sale sale, Product product, double qty) throws ApplicationException {
        sale.addProductToSale(product, qty);
        sale = em.merge(sale);
        product = em.merge(product);
        return sale;
    }

    public Iterable<Product> getProducts(int id) throws ApplicationException {
        Sale s = getSaleById(id);
        List<SaleProduct> products = s.getSaleProducts();
        return products.stream().map(SaleProduct::getProduct).collect(Collectors.toList());
    }

    public double[] closeSale(int id) throws ApplicationException {
        Sale sale = getSaleById(id);
        if (sale.getStatus().equals(SaleStatus.CLOSED)) {
            throw new ApplicationException("The sale with id " + id + " is already closed");
        } else if (sale.getStatus().equals(SaleStatus.CANCELED)) {
            throw new ApplicationException("The sale with id " + id + " is already canceled");
        } else {
            try {
                Customer c = sale.getCustomer();
                List<SaleProduct> products = sale.getSaleProducts();
                double total = sale.total();
                double discount = sale.discount();
                sale.setStatus(SaleStatus.CLOSED);
                em.merge(sale);
                double tNd[] = new double[2];
                tNd[0] = sale.total();
                tNd[1] = sale.discount();
                return tNd;
            } catch (Exception var9) {
                throw new ApplicationException("Couldn\'t close sale.");
            }
        }
    }

    public double[] cancelSale(int id) throws ApplicationException {
        Sale sale = getSaleById(id);
        if (sale.getStatus().equals(SaleStatus.CANCELED)) {
            throw new ApplicationException("The sale with id " + id + " is already canceled");
        } else {
            if (sale.getStatus().equals(SaleStatus.OPEN)) {
                Customer c = sale.getCustomer();
                List<SaleProduct> products = sale.getSaleProducts();
                double total = sale.total();
                double discount = sale.discount();
                for (SaleProduct sp : products) {
                    Product p = sp.getProduct();
                    p.setQty(p.getQty() + sp.getQty());
                    products.remove(p);
                    em.merge(p);

                }
                sale.removeSaleProducts();
                sale.setStatus(SaleStatus.CANCELED);
                em.merge(sale);
                double tNd[] = new double[2];
                tNd[0] = sale.total();
                tNd[1] = sale.discount();
                return tNd;
            }
        }
        throw new ApplicationException("Couldn\'t close sale.");
    }


    public Product getProduct(int prodCode) throws ApplicationException{
        TypedQuery<Product> query = em.createNamedQuery(Product.FIND_BY_PRODUCT_CODE, Product.class);
        query.setParameter(Product.PRODUCT_CODE, prodCode);
        try {
            return query.getSingleResult();
        } catch (Exception e) {
            throw new ApplicationException("Product not found");
        }
    }

    public Iterable<Product> getProducts() throws ApplicationException{
        TypedQuery<Product> query = em.createNamedQuery(Product.FIND_ALL, Product.class);
        try {
            return new LinkedList<>(query.getResultList());
        } catch (Exception e) {
            throw new ApplicationException("Couldn't get products");
        }
    }

	public List<SaleProduct> getSaleProducts(int saleId) throws ApplicationException {
		Sale sale = getSaleById(saleId);
		List<SaleProduct> sp = sale.getSaleProducts();
		return sp;
	}
}