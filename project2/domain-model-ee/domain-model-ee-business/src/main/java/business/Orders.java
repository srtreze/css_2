package business;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.EnumType.STRING;

/**
 * A order
 *
 * Created by Telma on 07/12/16.
 */

@Entity
@NamedQueries({
        @NamedQuery(name=Orders.FIND_BY_PROV, query="SELECT o FROM Orders o WHERE o.provider = :" +
                Orders.PROV),
        @NamedQuery(name=Orders.FIND_BY_PROD, query="SELECT o FROM Orders o WHERE o.product = :" +
                Orders.PROD),
        @NamedQuery(name=Orders.FIND_BY_PROV_AND_PROD, query="SELECT o FROM Orders o WHERE o.provider = :" +
                Orders.PROV + " AND o.product = :" + Orders.PROD),
        @NamedQuery(name=Orders.FIND_ALL_ORDERS, query="SELECT o FROM Orders o"),
        @NamedQuery(name=Orders.FIND_PENDENT_BY_PROD, query="SELECT o FROM Orders o WHERE o.product = :" +
                Orders.PROD + " AND o.status = :" + Orders.STATUS),
        @NamedQuery(name=Orders.FIND_PENDENT_BY_PROV, query="SELECT o FROM Orders o WHERE o.provider = :" +
                Orders.PROV + " AND o.status = :" + Orders.STATUS)
})
public class Orders {

    // Named query name constants
    public static final String FIND_BY_PROV = "Order.findByProvider";
    public static final String FIND_BY_PROD = "Order.findByProduct";
    public static final String FIND_BY_PROV_AND_PROD = "Order.findByProviderAndProduct";
    public static final String FIND_PENDENT_BY_PROD = "Order.findPendentByProduct";
    public static final String FIND_PENDENT_BY_PROV = "Order.findPendentByProvider";
    public static final String PROV = "provider";
    public static final String PROD = "product";
    public static final String STATUS = "status";
    public static final String FIND_ALL_ORDERS = "Order.findAllOrders";

    /**
     * Order primary key. Needed by JPA. Notice that it is not part of the
     * original domain model.
     */
    @Id
    @GeneratedValue
    private int id;

    /**
     * The date the order was made
     */
    @Temporal(TemporalType.DATE) private Date orderDate;

    /**
     * The date the order was delivered
     */
    @Temporal(TemporalType.DATE) private Date deliveredDate;

    /**
     * The date the order is supposed to be delivered
     */
    @Temporal(TemporalType.DATE) private Date expectedDate;

    /**
     * Whether the order is partial, delivered or pendent.
     */
    @Enumerated(STRING) private OrderStatus status;

    @Column private double qty;

    @ManyToOne private Provider provider;

    @OneToOne private Product product;

    @Column(nullable = false) private int original_order;




    // 1. constructor

    /**
     * Constructor needed by JPA.
     */
    protected Orders () {
    }

    public Orders(Provider provider, Product product, Date expectedDate, double qty) {
        this.expectedDate= expectedDate;
        this.product= product;
        this.provider= provider;
        this.qty = qty;
        this.orderDate = new Date();
        status=OrderStatus.PENDENT;
        original_order = 0;
    }


    // 2. getters and setters

    public int getId() {
        return this.id;
    }

    public double getQty() {
        return this.qty;
    }

    public int getOriginalO() {
        return this.original_order;
    }
    
    public void setOriginalO(int original) {
    	original_order=original;
    }

    public Provider getProvider() {
        return this.provider;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product prod) {
        this.product = prod;
    }

    public Date getDateOrder() {
        return this.orderDate;
    }

    public void setDateOrder(Date date) {
        this.orderDate = date;
    }

    public Date getExpectedDate() {
        return this.expectedDate;
    }

    public Date getDeliveredDate() {
        return this.deliveredDate;
    }

    public void setDeliveredDate(Date date) {
        this.deliveredDate = date;
    }

    public OrderStatus getStatus() {
        return this.status;
    }

    public void setStatus(OrderStatus newStatus) {
        this.status = newStatus;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

}
