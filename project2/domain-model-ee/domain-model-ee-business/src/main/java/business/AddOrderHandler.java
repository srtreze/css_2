package business;

import facade.dto.OrderDTO;
import facade.dto.ProductDTO;
import facade.dto.ProviderDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IAddOrderHandlerRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Telma on 08/12/16.
 */
@Stateless
@WebService
public class AddOrderHandler implements IAddOrderHandlerRemote {


    /**
     * The Order's catalog
     */
    @EJB
    private OrderCatalog orderCatalog;


    public ProviderDTO getProvider(int providerCode) throws ApplicationException {
    	try {
			Provider p = orderCatalog.getProvider(providerCode);
			return new ProviderDTO(providerCode, p.getProviderName());
		} catch (Exception e) {
            throw new ApplicationException ("Error getting provider with code" + providerCode, e);
		}
    }

    public ProductDTO getProduct(int productCode) throws ApplicationException {
    	try {
			Product p = orderCatalog.getProduct(productCode);
			return new ProductDTO(productCode, p.getDescription(), p.getFaceValue(), p.getQty(), p.getPendentQty());
		} catch (Exception e) {
            throw new ApplicationException ("Error getting provider with code" + productCode, e);
		}
    }

    /**
     * Adds a new order
     *
     * @param productCode The product
     * @param expectedDate The expected date
     * @param qty The qty ordered
     * @throws ApplicationException When the sale is already in the repository
     */
    public OrderDTO addOrder (int productCode, Date expectedDate, double qty, int provCode)
            throws ApplicationException {
        try {
            Orders o = orderCatalog.addOrder(provCode, productCode, expectedDate, qty);
            return new OrderDTO(o.getProduct().getProdCod(), o.getId(), o.getProvider().getProviderName(),
					o.getDateOrder(), o.getQty(), o.getExpectedDate(), false);

        } catch (Exception e) {
            throw new ApplicationException ("Error adding order of provider with code" + provCode, e);
        }
    }

}
