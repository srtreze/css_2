package business;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import facade.dto.OrderDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IViewOrderHandlerRemote;

@Stateless
@WebService
public class ViewOrderHandler implements IViewOrderHandlerRemote {
	
    /**
     * The Order's catalog
     */
    @EJB
    private OrderCatalog orderCatalog;

    public List<OrderDTO> getPendentProd(int prodCode) throws ApplicationException{
    	try {
    		Product p = orderCatalog.getProduct(prodCode);
			Iterable<Orders> o = orderCatalog.getPendentOrdersByProd(p);
			List<OrderDTO> oDTO = new LinkedList<>();
			boolean flag = true;
			for (Orders order : o) {
				if(order.getOriginalO() == 0){
					flag = false;
				}
				oDTO.add(new OrderDTO(order.getProduct().getProdCod(), order.getId(), order.getProvider().getProviderName(),
						order.getDateOrder(), order.getQty(), order.getExpectedDate(), flag));
				flag=true;
			}
			return oDTO;
		} catch (Exception e) {
			throw new ApplicationException("Error loading orders", e);
		}
    }
    
    
}
