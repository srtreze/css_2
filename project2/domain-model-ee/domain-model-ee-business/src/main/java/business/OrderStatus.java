package business;

/**
 * Created by Telma on 07/12/16.
 */
public enum OrderStatus {
	PENDENT, DELIVERED, PARTIAL;
}
