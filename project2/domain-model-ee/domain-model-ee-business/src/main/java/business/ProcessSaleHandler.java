package business;

import facade.dto.OrderDTO;
import facade.dto.ProductDTO;
import facade.dto.SaleDTO;
import facade.dto.SaleProductDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IProcessSaleHandlerRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Telma on 08/12/16.
 */
@Stateless
@WebService
public class ProcessSaleHandler implements IProcessSaleHandlerRemote {


    /**
     * The sale's catalog
     */
    @EJB
    private SaleCatalog saleCatalog;

    /**
     * The customer's catalog
     */
    @EJB
    private CustomerCatalog customerCatalog;

    /**
     * Adds a new sale
     *
     * @throws ApplicationException When the sale is already in the repository
     */
    public SaleDTO newSale (Date datee, int customer) throws ApplicationException {
        try {
            Customer c = customerCatalog.getCustomer(customer);
            Sale s = saleCatalog.addSale(datee, c);
            return new SaleDTO(s.getDate(), customer, s.getId());
        } catch (Exception e) {
            throw new ApplicationException ("Error adding sale of customer with vat " + customer , e);
        }
    }

    public void addProductToSale (int saleId, int productCode, double qty) throws ApplicationException {
        try {
            Product p = saleCatalog.getProduct(productCode);
            Sale s = saleCatalog.getSaleById(saleId);
            saleCatalog.addProductToSale(s, p,qty);
        } catch (Exception e) {
            throw new ApplicationException ("Error adding product with code" + productCode + " to sale with id " + saleId, e);
        }
    }

    public List<SaleProductDTO> getProducts(int saleId) throws ApplicationException {
        try {
            List<SaleProductDTO> spDTO = new LinkedList<>();
            Iterable<SaleProduct> sp = saleCatalog.getSaleProducts(saleId);
            for (SaleProduct saleProduct : sp) {
				spDTO.add(new SaleProductDTO(saleId, saleProduct.getProduct().getProdCod(), saleProduct.getQty()));
			}
            return spDTO;
        } catch (Exception e) {
            throw new ApplicationException ("Error getting products of sale with id " + saleId, e);
        }
    }

    public double[] closeSale(int saleId) throws ApplicationException{
        try {
            return saleCatalog.closeSale(saleId);
        } catch (Exception e) {
            throw new ApplicationException("Error closing sale with id "+saleId, e);
        }
    }

    public double[] cancelSale(int saleId) throws ApplicationException{
        try {
        	return saleCatalog.cancelSale(saleId);
        } catch (Exception e) {
            throw new ApplicationException("Error canceling sale with id "+saleId, e);
        }
    }

}
