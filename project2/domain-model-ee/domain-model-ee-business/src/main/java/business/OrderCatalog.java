package business;

import facade.exceptions.ApplicationException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by Telma on 07/12/16.
 */
@Stateless
public class OrderCatalog {

    /**
     * Entity manager for accessing the persistence service
     */
    @PersistenceContext
    private EntityManager em;



    /**
     * Finds an order given its product.
     *
     * @param prod
     * @return The Order object corresponding to the order with the product.
     * @throws ApplicationException When the Order with the product is not found.
     */
    public Orders getOrderByProd (Product prod) throws ApplicationException {
        TypedQuery<Orders> query = em.createNamedQuery(Orders.FIND_BY_PROD, Orders.class);
        query.setParameter(Orders.PROD, prod);
        try {
            return query.getSingleResult();
        } catch (PersistenceException e) {
            throw new ApplicationException ("Order not found.");
        }
    }


    /**
     * Finds an order given its provider.
     *
     * @param prov
     * @return The Order object corresponding to the order with the provider.
     * @throws ApplicationException When the Order with the provider is not found.
     */
    public Orders getOrderByProv (Provider prov) throws ApplicationException {
        TypedQuery<Orders> query = em.createNamedQuery(Orders.FIND_BY_PROV, Orders.class);
        query.setParameter(Orders.PROV, prov);
        try {
            return query.getSingleResult();
        } catch (PersistenceException e) {
            throw new ApplicationException ("Order not found.");
        }
    }


    /**
     * Adds a new order
     *
     * @param provider The provider
     * @param product The product
     * @param expectedDate The expected date
     * @param qty The order qty
     * @throws ApplicationException When the order is already in the repository
     */
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public Orders addOrder (int providerCode, int productCode, Date expectedDate, double qty) throws ApplicationException {
    	Product product = getProduct(productCode);
    	Provider provider = getProvider(providerCode);
        Orders order = new Orders(provider, product, expectedDate, qty);
        em.persist(order);
        product.setPendentQty(qty+product.getPendentQty());
        em.merge(product);
        return order;
    }

    public Orders getOrderById(int id) throws ApplicationException {
        Orders o = em.find(Orders.class, id);
        if (o == null)
            throw new ApplicationException("Order with id " + id + " not found");
        else
            return o;
    }

    public Iterable<Orders> getOrders() {
        TypedQuery<Orders> query = em.createNamedQuery(Orders.FIND_ALL_ORDERS, Orders.class);
        return new LinkedList<>(query.getResultList());
    }
    
    public Product getProduct(int prodCode) throws ApplicationException{
    	TypedQuery<Product> query = em.createNamedQuery(Product.FIND_BY_PRODUCT_CODE, Product.class);
    	query.setParameter(Product.PRODUCT_CODE, prodCode);
    	try {
        	return query.getSingleResult();
		} catch (Exception e) {
			throw new ApplicationException("Product not found");
		}
    }
    
    public Iterable<Product> getProducts() throws ApplicationException{
    	TypedQuery<Product> query = em.createNamedQuery(Product.FIND_ALL, Product.class);
    	try {
        	return new LinkedList<>(query.getResultList());
		} catch (Exception e) {
			throw new ApplicationException("Couldn't get products");
		}
    }
    
    public Provider getProvider(int provCode) throws ApplicationException{
    	TypedQuery<Provider> query = em.createNamedQuery(Provider.FIND_BY_CODE, Provider.class);
    	query.setParameter(Provider.CODE, provCode);
    	try {
        	return query.getSingleResult();
		} catch (Exception e) {
			throw new ApplicationException("Product not found");
		}
    }

    public void deleteOrder(int id) throws ApplicationException {
        Orders o = em.find(Orders.class, id);
        if (o == null)
            throw new ApplicationException("Order with id " + id + " not found");
        em.remove(o);
    }

    public Iterable<Orders> getPendentOrdersByProd(Product prod) throws ApplicationException {
    	TypedQuery<Orders> orders = em.createNamedQuery(Orders.FIND_PENDENT_BY_PROD, Orders.class);
    	orders.setParameter(Orders.PROD, prod);
    	orders.setParameter(Orders.STATUS, OrderStatus.PENDENT);
		try {
	        return new LinkedList<>(orders.getResultList());
		} catch (PersistenceException e) {
			throw new ApplicationException ("Customer not found.");
		}
    }

    public Iterable<Orders> getPartialOrdersByProd(Product prod) throws ApplicationException {
    	TypedQuery<Orders> orders = em.createNamedQuery(Orders.FIND_BY_PROD, Orders.class);
    	orders.setParameter(Orders.PROD, prod);
		try {
	        return new LinkedList<>(orders.getResultList());
		} catch (PersistenceException e) {
			throw new ApplicationException ("Customer not found.");
		}
    }

    public Iterable<Orders> getPendentOrdersByProv(Provider prov) throws ApplicationException {
    	TypedQuery<Orders> orders = em.createNamedQuery(Orders.FIND_PENDENT_BY_PROV, Orders.class);
    	orders.setParameter(Orders.PROV, prov);
    	orders.setParameter(Orders.STATUS, OrderStatus.PENDENT);
		try {
			
	        return new LinkedList<>(orders.getResultList());
		} catch (PersistenceException e) {
			throw new ApplicationException ("Customer not found.");
		}
    }
    
    public Iterable<Orders> getPartialOrdersByProv(Provider prov) throws ApplicationException {
    	TypedQuery<Orders> orders = em.createNamedQuery(Orders.FIND_BY_PROV, Orders.class);
    	orders.setParameter(Orders.PROD, prov);
		try {
	        return new LinkedList<>(orders.getResultList());
		} catch (PersistenceException e) {
			throw new ApplicationException ("Customer not found.");
		}
    }

    public void receiveOrder(int id, double qty) throws ApplicationException {
    	try {
        	Orders order = getOrderById(id);
        	Product product = order.getProduct();
        	double pendentQty = product.getPendentQty();
        	Date date = new Date();
        	double order_PendingQty = order.getQty();
        	if (order_PendingQty >= qty) {
        		product.setPendentQty(pendentQty - qty);
        		product.setQty(product.getQty()+qty);
        		em.merge(product);
        		if (order_PendingQty > qty) {
        			order.setStatus(OrderStatus.PARTIAL);
        			order.setDeliveredDate(date);
        			em.merge(order);
        			Orders order2 = new Orders(order.getProvider(), product, order.getExpectedDate(), order_PendingQty-qty);
        			order2.setOriginalO(order.getId());
        			order2.setDateOrder(order.getDateOrder());
        			em.persist(order2);
    			} else {
        			order.setStatus(OrderStatus.DELIVERED);
        			order.setDeliveredDate(date);
        			em.merge(order);
    			}
    		}
        } catch (Exception e) {
        	 throw new ApplicationException("Quantity received needs to be less than, or equal, to de pendent quantity | "+id+" - "+qty);
		}
    }
}
