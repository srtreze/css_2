package business;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import facade.dto.OrderDTO;
import facade.dto.ProductDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IReceiveOrderHandlerRemote;

@Stateless
@WebService
public class ReceiveOrderHandler implements IReceiveOrderHandlerRemote {


    /**
     * The Order's catalog
     */
    @EJB
    private OrderCatalog orderCatalog;

    public List<OrderDTO> receiveOrder(int provCode) throws ApplicationException{
    	try {
    		Provider p = orderCatalog.getProvider(provCode);
			Iterable<Orders> o = orderCatalog.getPendentOrdersByProv(p);
			List<OrderDTO> oDTO = new LinkedList<>();
			
			boolean flag = true;
			for (Orders order : o) {
				if(order.getOriginalO() == 0){
					flag = false;
				}
				oDTO.add(new OrderDTO(order.getProduct().getProdCod(), order.getId(), order.getProvider().getProviderName(),
						order.getDateOrder(), order.getQty(), order.getExpectedDate(), flag));
				flag=true;
			}
			return oDTO;
		} catch (Exception e) {
			throw new ApplicationException("Error loading orders to provider "+provCode, e);
		}
    }

    public void processOrder(int orderId, double qty) throws ApplicationException{
    	try {
    		orderCatalog.receiveOrder(orderId, qty);
    	} catch (Exception e) {
			throw new ApplicationException("Error processing order "+orderId, e);
		}
    }
    
}
