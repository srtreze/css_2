package facade.handlers;

import business.Product;
import business.Provider;
import facade.dto.OrderDTO;
import facade.dto.ProductDTO;
import facade.dto.ProviderDTO;
import facade.exceptions.ApplicationException;

import javax.ejb.Remote;
import java.util.Date;


@Remote
public interface IAddOrderHandlerRemote {

	public ProviderDTO getProvider(int providerCode) throws ApplicationException;
	
	public ProductDTO getProduct(int productCode) throws ApplicationException;

	public OrderDTO addOrder (int productCode, Date expectedDate, double qty, int provCode) throws ApplicationException;
}
