package facade.handlers;

import java.util.Iterator;

import javax.ejb.Remote;

import facade.dto.CustomerDTO;
import facade.exceptions.ApplicationException;

@Remote
public interface IRemoveCustomerHandlerRemote {

	public CustomerDTO getCustomer(int id) throws ApplicationException;

	public Iterable<CustomerDTO> getCustomers();

	public void deleteCustomer(int id) throws ApplicationException;
}
