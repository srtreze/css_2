package facade.handlers;

import java.util.Iterator;

import javax.ejb.Remote;

import facade.dto.OrderDTO;
import facade.exceptions.ApplicationException;

@Remote
public interface IViewOrderHandlerRemote {

	public Iterable<OrderDTO> getPendentProd(int prodCode) throws ApplicationException;
	
	
}
