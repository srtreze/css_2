package facade.handlers;

import facade.dto.SaleDTO;
import facade.dto.SaleProductDTO;
import facade.exceptions.ApplicationException;

import javax.ejb.Remote;
import java.util.Date;

/**
 * Created by Telma on 19/12/16.
 */

@Remote
public interface IProcessSaleHandlerRemote {

    public SaleDTO newSale(Date datee, int customer) throws ApplicationException;

    public Iterable<SaleProductDTO> getProducts(int saleId) throws ApplicationException;

    public void addProductToSale (int saleId, int productCode, double qty) throws ApplicationException;

    public double[] closeSale(int saleId) throws ApplicationException;

    public double[] cancelSale(int saleId) throws ApplicationException;
}
