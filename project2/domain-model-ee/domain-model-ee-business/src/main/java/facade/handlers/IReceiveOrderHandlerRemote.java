package facade.handlers;

import java.util.Iterator;

import javax.ejb.Remote;

import facade.dto.OrderDTO;
import facade.exceptions.ApplicationException;

@Remote
public interface IReceiveOrderHandlerRemote {

	public Iterable<OrderDTO> receiveOrder(int provCode) throws ApplicationException;
	
	public void processOrder(int id, double qty) throws ApplicationException;
}
