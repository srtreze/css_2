package facade.interfaces;

import java.io.Serializable;

public interface IProvider extends Serializable {

    int getProviderCode();

    String getProviderName();

    int getProviderPhoneNumber();

    int getProviderId();

}