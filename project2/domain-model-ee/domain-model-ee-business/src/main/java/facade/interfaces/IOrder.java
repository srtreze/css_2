package facade.interfaces;

import java.io.Serializable;
import java.util.Date;

public interface IOrder extends Serializable {

    int getProviderCode();

    int getProductCode();

    double getQty();

    Date getExpectedDate();

    Date getOrderDate();

    int getId();

    boolean getHasPrev();

}