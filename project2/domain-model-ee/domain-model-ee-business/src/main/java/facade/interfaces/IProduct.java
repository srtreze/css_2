package facade.interfaces;

import java.io.Serializable;

public interface IProduct extends Serializable{

	int getProductCode();
	
	String getDescription();
	
	double getFaceValue();
	
	double getQty();
	
	double getPendentQty();
}
