package facade.dto;

import business.Product;
import business.Provider;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Telma on 08/12/16.
 */
public class OrderDTO implements Serializable {

    private static final long serialVersionUID = -4087131153704256744L;
    
	public final int productCode;
    public final int id;
    public final String provDesig;
    public final Date orderDate;
    public final double qty;
    public final Date expectedDate;
    public final boolean hasPrev;

    public OrderDTO(int productCode, int id, String provDesig, Date orderDate, double qty, Date expectedDate,
			boolean hasPrev) {
		super();
		this.productCode = productCode;
		this.id = id;
		this.provDesig = provDesig;
		this.orderDate = orderDate;
		this.qty = qty;
		this.expectedDate = expectedDate;
		this.hasPrev = hasPrev;
	}
}
