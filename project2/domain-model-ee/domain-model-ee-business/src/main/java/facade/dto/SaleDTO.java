package facade.dto;

import business.*;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Telma on 08/12/16.
 */
public class SaleDTO implements Serializable {

    private static final long serialVersionUID = -4087131153704256744L;

    public final Date date;
    public final int customer;
    public final String status;
    public final int id;
    public final List<SaleProductDTO> saleProducts;

    public SaleDTO(Date date, int customer, int id) {
        this.date = date;
        this.customer = customer;
        this.status = "OPEN";
        this.saleProducts = new LinkedList<SaleProductDTO>();
        this.id=id;
    }
}
