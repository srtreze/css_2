package facade.dto;

import java.io.Serializable;

public class ProductDTO implements Serializable {

	private static final long serialVersionUID = -4087131153704256744L;

	public final int prodCode;
	public final String description;
	public final double faceValue;
	public final double qty;
	public final double pendentQty;
	
	
	public ProductDTO(int prodCode, String description, double faceValue, double qty, double pendentQty) {
		super();
		this.prodCode = prodCode;
		this.description = description;
		this.faceValue = faceValue;
		this.qty = qty;
		this.pendentQty = pendentQty;
	}
}
