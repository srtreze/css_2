package facade.dto;

import java.io.Serializable;

/**
 * Created by Telma on 19/12/16.
 */
public class SaleProductDTO implements Serializable {

    private static final long serialVersionUID = -4087131153704256744L;

    public final int saleId;
    public final int productCode;
    public final double qty;


    public SaleProductDTO(int saleId, int productCode, double qty) {
        this.productCode = productCode;
        this.saleId = saleId;
        this.qty = qty;
    }
}
