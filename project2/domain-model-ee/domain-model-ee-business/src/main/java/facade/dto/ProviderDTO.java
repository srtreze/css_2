package facade.dto;

import java.io.Serializable;

/**
 * Created by Telma on 07/12/16.
 */
public class ProviderDTO implements Serializable{

    private static final long serialVersionUID = -4087131153704256744L;

    public final int code;
    public final String name;

    public ProviderDTO(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
