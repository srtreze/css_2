
package business;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the business package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddCustomerResponse_QNAME = new QName("http://business/", "addCustomerResponse");
    private final static QName _ApplicationException_QNAME = new QName("http://business/", "ApplicationException");
    private final static QName _AddCustomer_QNAME = new QName("http://business/", "addCustomer");
    private final static QName _GetDiscounts_QNAME = new QName("http://business/", "getDiscounts");
    private final static QName _GetDiscountsResponse_QNAME = new QName("http://business/", "getDiscountsResponse");
    private final static QName _GetProduct_QNAME = new QName("http://business/", "getProduct");
    private final static QName _GetProductResponse_QNAME = new QName("http://business/", "getProductResponse");
    private final static QName _AddOrderResponse_QNAME = new QName("http://business/", "addOrderResponse");
    private final static QName _GetProviderResponse_QNAME = new QName("http://business/", "getProviderResponse");
    private final static QName _AddOrder_QNAME = new QName("http://business/", "addOrder");
    private final static QName _GetProvider_QNAME = new QName("http://business/", "getProvider");
    private final static QName _GetCustomers_QNAME = new QName("http://business/", "getCustomers");
    private final static QName _GetCustomerResponse_QNAME = new QName("http://business/", "getCustomerResponse");
    private final static QName _GetCustomersResponse_QNAME = new QName("http://business/", "getCustomersResponse");
    private final static QName _DeleteCustomerResponse_QNAME = new QName("http://business/", "deleteCustomerResponse");
    private final static QName _GetCustomer_QNAME = new QName("http://business/", "getCustomer");
    private final static QName _DeleteCustomer_QNAME = new QName("http://business/", "deleteCustomer");
    private final static QName _GetPendentProd_QNAME = new QName("http://business/", "getPendentProd");
    private final static QName _GetPendentProdResponse_QNAME = new QName("http://business/", "getPendentProdResponse");
    private final static QName _ReceiveOrder_QNAME = new QName("http://business/", "receiveOrder");
    private final static QName _ProcessOrderResponse_QNAME = new QName("http://business/", "processOrderResponse");
    private final static QName _ProcessOrder_QNAME = new QName("http://business/", "processOrder");
    private final static QName _ReceiveOrderResponse_QNAME = new QName("http://business/", "receiveOrderResponse");
    private final static QName _NewSaleResponse_QNAME = new QName("http://business/", "newSaleResponse");
    private final static QName _CloseSale_QNAME = new QName("http://business/", "closeSale");
    private final static QName _CancelSale_QNAME = new QName("http://business/", "cancelSale");
    private final static QName _CancelSaleResponse_QNAME = new QName("http://business/", "cancelSaleResponse");
    private final static QName _GetProducts_QNAME = new QName("http://business/", "getProducts");
    private final static QName _NewSale_QNAME = new QName("http://business/", "newSale");
    private final static QName _AddProductToSale_QNAME = new QName("http://business/", "addProductToSale");
    private final static QName _CloseSaleResponse_QNAME = new QName("http://business/", "closeSaleResponse");
    private final static QName _GetProductsResponse_QNAME = new QName("http://business/", "getProductsResponse");
    private final static QName _AddProductToSaleResponse_QNAME = new QName("http://business/", "addProductToSaleResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: business
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddCustomer }
     * 
     */
    public AddCustomer createAddCustomer() {
        return new AddCustomer();
    }

    /**
     * Create an instance of {@link ApplicationException }
     * 
     */
    public ApplicationException createApplicationException() {
        return new ApplicationException();
    }

    /**
     * Create an instance of {@link AddCustomerResponse }
     * 
     */
    public AddCustomerResponse createAddCustomerResponse() {
        return new AddCustomerResponse();
    }

    /**
     * Create an instance of {@link GetDiscountsResponse }
     * 
     */
    public GetDiscountsResponse createGetDiscountsResponse() {
        return new GetDiscountsResponse();
    }

    /**
     * Create an instance of {@link GetDiscounts }
     * 
     */
    public GetDiscounts createGetDiscounts() {
        return new GetDiscounts();
    }

    /**
     * Create an instance of {@link CustomerDTO }
     * 
     */
    public CustomerDTO createCustomerDTO() {
        return new CustomerDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "addCustomerResponse")
    public JAXBElement<AddCustomerResponse> createAddCustomerResponse(AddCustomerResponse value) {
        return new JAXBElement<AddCustomerResponse>(_AddCustomerResponse_QNAME, AddCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "ApplicationException")
    public JAXBElement<ApplicationException> createApplicationException(ApplicationException value) {
        return new JAXBElement<ApplicationException>(_ApplicationException_QNAME, ApplicationException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "addCustomer")
    public JAXBElement<AddCustomer> createAddCustomer(AddCustomer value) {
        return new JAXBElement<AddCustomer>(_AddCustomer_QNAME, AddCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDiscounts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getDiscounts")
    public JAXBElement<GetDiscounts> createGetDiscounts(GetDiscounts value) {
        return new JAXBElement<GetDiscounts>(_GetDiscounts_QNAME, GetDiscounts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDiscountsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getDiscountsResponse")
    public JAXBElement<GetDiscountsResponse> createGetDiscountsResponse(GetDiscountsResponse value) {
        return new JAXBElement<GetDiscountsResponse>(_GetDiscountsResponse_QNAME, GetDiscountsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link GetProduct }
     * 
     */
    public GetProduct createGetProduct() {
        return new GetProduct();
    }

    /**
     * Create an instance of {@link GetProductResponse }
     * 
     */
    public GetProductResponse createGetProductResponse() {
        return new GetProductResponse();
    }

    /**
     * Create an instance of {@link AddOrder }
     * 
     */
    public AddOrder createAddOrder() {
        return new AddOrder();
    }

    /**
     * Create an instance of {@link GetProvider }
     * 
     */
    public GetProvider createGetProvider() {
        return new GetProvider();
    }

    /**
     * Create an instance of {@link AddOrderResponse }
     * 
     */
    public AddOrderResponse createAddOrderResponse() {
        return new AddOrderResponse();
    }

    /**
     * Create an instance of {@link GetProviderResponse }
     * 
     */
    public GetProviderResponse createGetProviderResponse() {
        return new GetProviderResponse();
    }

    /**
     * Create an instance of {@link OrderDTO }
     * 
     */
    public OrderDTO createOrderDTO() {
        return new OrderDTO();
    }

    /**
     * Create an instance of {@link ProductDTO }
     * 
     */
    public ProductDTO createProductDTO() {
        return new ProductDTO();
    }

    /**
     * Create an instance of {@link ProviderDTO }
     * 
     */
    public ProviderDTO createProviderDTO() {
        return new ProviderDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getProduct")
    public JAXBElement<GetProduct> createGetProduct(GetProduct value) {
        return new JAXBElement<GetProduct>(_GetProduct_QNAME, GetProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getProductResponse")
    public JAXBElement<GetProductResponse> createGetProductResponse(GetProductResponse value) {
        return new JAXBElement<GetProductResponse>(_GetProductResponse_QNAME, GetProductResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "addOrderResponse")
    public JAXBElement<AddOrderResponse> createAddOrderResponse(AddOrderResponse value) {
        return new JAXBElement<AddOrderResponse>(_AddOrderResponse_QNAME, AddOrderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProviderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getProviderResponse")
    public JAXBElement<GetProviderResponse> createGetProviderResponse(GetProviderResponse value) {
        return new JAXBElement<GetProviderResponse>(_GetProviderResponse_QNAME, GetProviderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "addOrder")
    public JAXBElement<AddOrder> createAddOrder(AddOrder value) {
        return new JAXBElement<AddOrder>(_AddOrder_QNAME, AddOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProvider }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getProvider")
    public JAXBElement<GetProvider> createGetProvider(GetProvider value) {
        return new JAXBElement<GetProvider>(_GetProvider_QNAME, GetProvider.class, null, value);
    }

    /**
     * Create an instance of {@link GetCustomerResponse }
     * 
     */
    public GetCustomerResponse createGetCustomerResponse() {
        return new GetCustomerResponse();
    }

    /**
     * Create an instance of {@link GetCustomersResponse }
     * 
     */
    public GetCustomersResponse createGetCustomersResponse() {
        return new GetCustomersResponse();
    }

    /**
     * Create an instance of {@link GetCustomers }
     * 
     */
    public GetCustomers createGetCustomers() {
        return new GetCustomers();
    }

    /**
     * Create an instance of {@link DeleteCustomer }
     * 
     */
    public DeleteCustomer createDeleteCustomer() {
        return new DeleteCustomer();
    }

    /**
     * Create an instance of {@link DeleteCustomerResponse }
     * 
     */
    public DeleteCustomerResponse createDeleteCustomerResponse() {
        return new DeleteCustomerResponse();
    }

    /**
     * Create an instance of {@link GetCustomer }
     * 
     */
    public GetCustomer createGetCustomer() {
        return new GetCustomer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getCustomers")
    public JAXBElement<GetCustomers> createGetCustomers(GetCustomers value) {
        return new JAXBElement<GetCustomers>(_GetCustomers_QNAME, GetCustomers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getCustomerResponse")
    public JAXBElement<GetCustomerResponse> createGetCustomerResponse(GetCustomerResponse value) {
        return new JAXBElement<GetCustomerResponse>(_GetCustomerResponse_QNAME, GetCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getCustomersResponse")
    public JAXBElement<GetCustomersResponse> createGetCustomersResponse(GetCustomersResponse value) {
        return new JAXBElement<GetCustomersResponse>(_GetCustomersResponse_QNAME, GetCustomersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "deleteCustomerResponse")
    public JAXBElement<DeleteCustomerResponse> createDeleteCustomerResponse(DeleteCustomerResponse value) {
        return new JAXBElement<DeleteCustomerResponse>(_DeleteCustomerResponse_QNAME, DeleteCustomerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getCustomer")
    public JAXBElement<GetCustomer> createGetCustomer(GetCustomer value) {
        return new JAXBElement<GetCustomer>(_GetCustomer_QNAME, GetCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "deleteCustomer")
    public JAXBElement<DeleteCustomer> createDeleteCustomer(DeleteCustomer value) {
        return new JAXBElement<DeleteCustomer>(_DeleteCustomer_QNAME, DeleteCustomer.class, null, value);
    }

    /**
     * Create an instance of {@link GetPendentProd }
     * 
     */
    public GetPendentProd createGetPendentProd() {
        return new GetPendentProd();
    }

    /**
     * Create an instance of {@link GetPendentProdResponse }
     * 
     */
    public GetPendentProdResponse createGetPendentProdResponse() {
        return new GetPendentProdResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPendentProd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getPendentProd")
    public JAXBElement<GetPendentProd> createGetPendentProd(GetPendentProd value) {
        return new JAXBElement<GetPendentProd>(_GetPendentProd_QNAME, GetPendentProd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPendentProdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "getPendentProdResponse")
    public JAXBElement<GetPendentProdResponse> createGetPendentProdResponse(GetPendentProdResponse value) {
        return new JAXBElement<GetPendentProdResponse>(_GetPendentProdResponse_QNAME, GetPendentProdResponse.class, null, value);
    }
    /**
     * Create an instance of {@link ProcessOrder }
     * 
     */
    public ProcessOrder createProcessOrder() {
        return new ProcessOrder();
    }

    /**
     * Create an instance of {@link ProcessOrderResponse }
     * 
     */
    public ProcessOrderResponse createProcessOrderResponse() {
        return new ProcessOrderResponse();
    }

    /**
     * Create an instance of {@link ReceiveOrder }
     * 
     */
    public ReceiveOrder createReceiveOrder() {
        return new ReceiveOrder();
    }

    /**
     * Create an instance of {@link ReceiveOrderResponse }
     * 
     */
    public ReceiveOrderResponse createReceiveOrderResponse() {
        return new ReceiveOrderResponse();
    }


    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "receiveOrder")
    public JAXBElement<ReceiveOrder> createReceiveOrder(ReceiveOrder value) {
        return new JAXBElement<ReceiveOrder>(_ReceiveOrder_QNAME, ReceiveOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "processOrderResponse")
    public JAXBElement<ProcessOrderResponse> createProcessOrderResponse(ProcessOrderResponse value) {
        return new JAXBElement<ProcessOrderResponse>(_ProcessOrderResponse_QNAME, ProcessOrderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "processOrder")
    public JAXBElement<ProcessOrder> createProcessOrder(ProcessOrder value) {
        return new JAXBElement<ProcessOrder>(_ProcessOrder_QNAME, ProcessOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://business/", name = "receiveOrderResponse")
    public JAXBElement<ReceiveOrderResponse> createReceiveOrderResponse(ReceiveOrderResponse value) {
        return new JAXBElement<ReceiveOrderResponse>(_ReceiveOrderResponse_QNAME, ReceiveOrderResponse.class, null, value);
    }
    

        /**
         * Create an instance of {@link CancelSale }
         * 
         */
        public CancelSale createCancelSale() {
            return new CancelSale();
        }

        /**
         * Create an instance of {@link CancelSaleResponse }
         * 
         */
        public CancelSaleResponse createCancelSaleResponse() {
            return new CancelSaleResponse();
        }

        /**
         * Create an instance of {@link CloseSale }
         * 
         */
        public CloseSale createCloseSale() {
            return new CloseSale();
        }


        /**
         * Create an instance of {@link NewSaleResponse }
         * 
         */
        public NewSaleResponse createNewSaleResponse() {
            return new NewSaleResponse();
        }

        /**
         * Create an instance of {@link AddProductToSaleResponse }
         * 
         */
        public AddProductToSaleResponse createAddProductToSaleResponse() {
            return new AddProductToSaleResponse();
        }

        /**
         * Create an instance of {@link GetProductsResponse }
         * 
         */
        public GetProductsResponse createGetProductsResponse() {
            return new GetProductsResponse();
        }

        /**
         * Create an instance of {@link GetProducts }
         * 
         */
        public GetProducts createGetProducts() {
            return new GetProducts();
        }

        /**
         * Create an instance of {@link NewSale }
         * 
         */
        public NewSale createNewSale() {
            return new NewSale();
        }

        /**
         * Create an instance of {@link AddProductToSale }
         * 
         */
        public AddProductToSale createAddProductToSale() {
            return new AddProductToSale();
        }

        /**
         * Create an instance of {@link CloseSaleResponse }
         * 
         */
        public CloseSaleResponse createCloseSaleResponse() {
            return new CloseSaleResponse();
        }

        /**
         * Create an instance of {@link SaleProduct }
         * 
         */
        public SaleProduct createSaleProduct() {
            return new SaleProduct();
        }

        /**
         * Create an instance of {@link SaleDTO }
         * 
         */
        public SaleDTO createSaleDTO() {
            return new SaleDTO();
        }

        /**
         * Create an instance of {@link SaleProductDTO }
         * 
         */
        public SaleProductDTO createSaleProductDTO() {
            return new SaleProductDTO();
        }

        /**
         * Create an instance of {@link Customer }
         * 
         */
        public Customer createCustomer() {
            return new Customer();
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link NewSaleResponse }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "newSaleResponse")
        public JAXBElement<NewSaleResponse> createNewSaleResponse(NewSaleResponse value) {
            return new JAXBElement<NewSaleResponse>(_NewSaleResponse_QNAME, NewSaleResponse.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link CloseSale }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "closeSale")
        public JAXBElement<CloseSale> createCloseSale(CloseSale value) {
            return new JAXBElement<CloseSale>(_CloseSale_QNAME, CloseSale.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link CancelSale }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "cancelSale")
        public JAXBElement<CancelSale> createCancelSale(CancelSale value) {
            return new JAXBElement<CancelSale>(_CancelSale_QNAME, CancelSale.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link CancelSaleResponse }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "cancelSaleResponse")
        public JAXBElement<CancelSaleResponse> createCancelSaleResponse(CancelSaleResponse value) {
            return new JAXBElement<CancelSaleResponse>(_CancelSaleResponse_QNAME, CancelSaleResponse.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link GetProducts }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "getProducts")
        public JAXBElement<GetProducts> createGetProducts(GetProducts value) {
            return new JAXBElement<GetProducts>(_GetProducts_QNAME, GetProducts.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link NewSale }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "newSale")
        public JAXBElement<NewSale> createNewSale(NewSale value) {
            return new JAXBElement<NewSale>(_NewSale_QNAME, NewSale.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link AddProductToSale }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "addProductToSale")
        public JAXBElement<AddProductToSale> createAddProductToSale(AddProductToSale value) {
            return new JAXBElement<AddProductToSale>(_AddProductToSale_QNAME, AddProductToSale.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link CloseSaleResponse }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "closeSaleResponse")
        public JAXBElement<CloseSaleResponse> createCloseSaleResponse(CloseSaleResponse value) {
            return new JAXBElement<CloseSaleResponse>(_CloseSaleResponse_QNAME, CloseSaleResponse.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link GetProductsResponse }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "getProductsResponse")
        public JAXBElement<GetProductsResponse> createGetProductsResponse(GetProductsResponse value) {
            return new JAXBElement<GetProductsResponse>(_GetProductsResponse_QNAME, GetProductsResponse.class, null, value);
        }

        /**
         * Create an instance of {@link JAXBElement }{@code <}{@link AddProductToSaleResponse }{@code >}}
         * 
         */
        @XmlElementDecl(namespace = "http://business/", name = "addProductToSaleResponse")
        public JAXBElement<AddProductToSaleResponse> createAddProductToSaleResponse(AddProductToSaleResponse value) {
            return new JAXBElement<AddProductToSaleResponse>(_AddProductToSaleResponse_QNAME, AddProductToSaleResponse.class, null, value);
        }

}