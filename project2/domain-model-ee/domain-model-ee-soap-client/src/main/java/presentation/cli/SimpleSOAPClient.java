package presentation.cli;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import business.*;

/**
 * A simple application client that uses both services.
 *	
 * @author fmartins
 * @version 1.2 (11/02/2015)
 * 
 */
public class SimpleSOAPClient {

	/**
	 * A simple interaction with the application services
	 * 
	 * @param args Command line parameters
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		// Make a service
	    AddCustomerHandlerService service = new AddCustomerHandlerService();
	    AddOrderHandlerService addOrderService = new AddOrderHandlerService();
	    ReceiveOrderHandlerService receiveOrderService = new ReceiveOrderHandlerService();
	    ViewOrderHandlerService viewOrderHandlerService = new ViewOrderHandlerService();
		ProcessSaleHandlerService processSaleService = new ProcessSaleHandlerService();

	    // Now use the service to get a stub which implements the SDI.
	    AddCustomerHandler customerHandler = service.getAddCustomerHandlerPort();
	    AddOrderHandler addOrderHandler = addOrderService.getAddOrderHandlerPort();
	    ReceiveOrderHandler receiveOrder = receiveOrderService.getReceiveOrderHandlerPort();
	    ViewOrderHandler viewOrderHandler = viewOrderHandlerService.getViewOrderHandlerPort();
		ProcessSaleHandler processSaleHandler = processSaleService.getProcessSaleHandlerPort();

	    // Make the actual call
	    try {
			CustomerDTO c = customerHandler.addCustomer(168027852, "Customer 1", 217500255, 2);
			//INSERT into Customer (vatNumber, designation, phoneNumber, discountType) values (168027852, 'Customer 1', 217500255, 1);
			
			Scanner scan = new Scanner(System.in);
			boolean condicion = true;
			while (condicion) {
				int op = menu();
				switch (op) {
				case 1:
					System.out.println("  Write the provider code:");
					int provCode1 = scan.nextInt();
					ProviderDTO p1 = addOrderHandler.getProvider(provCode1);
					System.out.println("  The provider with code "+p1.getCode()+" is "+p1.getName());
					System.out.println("  Write the product's code you wish to order:");
					int prodCode1 = scan.nextInt();
					ProductDTO prod1 = addOrderHandler.getProduct(prodCode1);
					System.out.println("  The product with code "+prodCode1+" is "+prod1.getDescription()+" and it's value is "+prod1.getFaceValue()
							+"\n  Quantity: "+prod1.getQty()+" | Pendent quantity "+prod1.getPendentQty());
					System.out.println("  Write the quantity you wish to order:");
					int qty1 = scan.nextInt();
					System.out.println("  Write the date you expect the order to arive:\n  Format is: dd-mm-yyy");
					String stringDate = scan.next();
					DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
					Date date = format.parse(stringDate);
					GregorianCalendar gc = new GregorianCalendar();
					XMLGregorianCalendar xgc;
					gc.setTime(date);
					try {
						xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
					} catch (Exception e) {
						throw new Exception(e);
					}
					addOrderHandler.addOrder(prodCode1, xgc, qty1, provCode1);
					break;

				case 2:
					System.out.println("  Write the provider code:");
					int provCode2 = scan.nextInt();
					Iterable<OrderDTO> io2 = receiveOrder.receiveOrder(provCode2);
					for (OrderDTO orderDTO : io2) {
						System.out.println("  "+orderDTO.getId()+" :"
								+ " Product code - "+orderDTO.getProductCode()+" |"
								+ " Quantity - "+orderDTO.getQty()+" |"
								+ " Ordered - "+orderDTO.getOrderDate()+" |"
								+ " Expected - "+orderDTO.getExpectedDate());
					}
					System.out.println("  Write the order's number:");
					int orderId = scan.nextInt();
					System.out.println("  Write the quantity you're receiving:");
					int qty2 = scan.nextInt();
					receiveOrder.processOrder(orderId, qty2);
					System.out.println("  Registered successfully.");
					break;
					
				case 3:
					System.out.println("  Write the product's code:");
					int prodCode3 = scan.nextInt();
					Iterable<OrderDTO> io3 = viewOrderHandler.getPendentProd(prodCode3);
					for (OrderDTO orderDTO : io3) {
						System.out.println("  "+orderDTO.getId()+" :"
								+ " Provider name - "+orderDTO.getProvDesig()+" |"
								+ " Quantity - "+orderDTO.getQty()+" |"
								+ " Ordered - "+orderDTO.getOrderDate()+" |"
								+ " Expected - "+orderDTO.getExpectedDate()+" |"
								+ " Has previous? - "+orderDTO.isHasPrev());
					}
					break;

				case 4:
					System.out.println("  Write the customer vat number:");
					System.out.println("  (e.g. 168027852)");
					int vat = scan.nextInt();
					Date datee = new Date();
					GregorianCalendar gcc = new GregorianCalendar();
					XMLGregorianCalendar xgcc;
					gcc.setTime(datee);
					try {
						xgcc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcc);
					} catch (Exception e) {
						throw new Exception(e);
					}
					SaleDTO sDTO = processSaleHandler.newSale(xgcc, vat);
					boolean condition1=true;
					while (condition1) {
						
						System.out.println("  The sale was created with id "+ sDTO.getId() + " with date "+ sDTO.getDate());
	                    System.out.println("\n  What do you want to do in your sale?"
	                            + "\n   |  a - add product   |"
	                            + "\n   |  b - close sale    |"
	                            + "\n   |  c - cancel sale   |"
	                            + "\n   ----------------------"
	                            + "\n\n  Write your option's number:");
	                    String option = scan.next();
	                        switch (option) {
	                            case "a":
	                                System.out.println("  Write the product's code you wish to add to sale:");
						            int prodCodee = scan.nextInt();
						            ProductDTO pp = addOrderHandler.getProduct(prodCodee);
						            System.out.println("  The product with code "+prodCodee+" is "+pp.getDescription()+" and it's value is "+pp.getFaceValue()
								        +"\n  Quantity: "+pp.getQty());
						            System.out.println("  Write the quantity you wish :");
						            int qtyy = scan.nextInt();
						            processSaleHandler.addProductToSale(sDTO.getId(), prodCodee, qtyy);
						            System.out.println("  The product was added to sale with id "+ sDTO.getId() + " with date ");
						            System.out.println("  The sale products are:");
						            Iterable<SaleProductDTO> sp = processSaleHandler.getProducts(sDTO.getId());
						            for (SaleProductDTO spDTO : sp) {
										System.out.println("  - Product code: "+spDTO.getProductCode()+" | Quantity: "+spDTO.getQty());
									}
						            break;
						            
	                            case "b":
	                                System.out.println("  Your sale will be close now");
	                                List<Double> tNd = processSaleHandler.closeSale(sDTO.getId());
	                                System.out.println("  Your sale was successfully closed");
	                                System.out.println("  Total: "+tNd.get(0)+" | Discount: "+tNd.get(1));
	                                System.out.println("  Final: "+(tNd.get(0)-tNd.get(1)));
	                                condition1=false;
	                                break;
	                                
	                            case "c":
	                                System.out.println("  Are you sure you want to cancel your sale? (yes or no)");
	                                String answer = scan.next();
	                                switch (answer) {
	                                    case "yes":
	                                        System.out.println("  Your sale will be cancel now");
	                                        List<Double> tNd1 = processSaleHandler.cancelSale(sDTO.getId());
	                                        System.out.println("  Your sale was successfully canceled");
	    	                                System.out.println("  Total: "+tNd1.get(0)+" | Discount: "+tNd1.get(1));
	    	                                System.out.println("  Final: "+(tNd1.get(0)-tNd1.get(1)));
	                                        break;
	                                    case "no":
	                                        break;
	                                }
	                                condition1=false;
	                                break;

	                            default:
	                                break;
	                        }
					}
                    break;


                case 0:
					condicion = false;
					break;

				default:
					break;
				}
			}
			
			
			
		} catch (ApplicationException_Exception e) {
			System.out.println("  There was a problem with the program.");
			System.out.println("  Cause: ");
			e.printStackTrace();
		}

	}
	
	public static int menu(){
		System.out.println("\n Welcome! What do you want to do?"
			+ "\n  |  1 - new order           |"
			+ "\n  |  2 - receive order       |"
			+ "\n  |  3 - view pending orders |"
			+ "\n  |  4 - new product sale    |"
			+ "\n  ----------------------------"
			+ "\n  |  0 - exit                |"
			+ "\n  ----------------------------"
			+ "\n\n  Write your option's number:");
		Scanner scan = new Scanner(System.in);
		return scan.nextInt();
	}
}
