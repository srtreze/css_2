<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  No scriptlets!!! 
	  See http://download.oracle.com/javaee/5/tutorial/doc/bnakc.html 
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="model" class="presentation.web.model.NewCustomerModel" scope="request"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="/resources/app.css"> 
<title>SaleSys: Create Client</title>
</head>
<body>
<h2 align="center">Create a new client</h2>
<form action="criarCliente" method="post" align="center">
    <div class="mandatory_field">
    	<label for="designacao">Name:</label>
    	<input type="text" name="designacao" size="50" value="${model.designation}" autofocus/> 
    </div>
    <div class="mandatory_field">
		<label for="npc">Client VAT Number:</label>
		<input type="text" name="npc" size="35" value="${model.VATNumber}"/>
    </div>
   <div class="optional_field">
   		<label for="telefone">Telephone Number:</label>
   		<input type="text" name="telefone" size="36" value="${model.phoneNumber}"/>
   </div>
   <div class="mandatory_field">
		<label for="desconto">Discount Type:</label>
		<select name="desconto">  
			<c:forEach var="desconto" items="${model.discounts}">
				<c:choose>
    				<c:when test="${model.discountType == desconto.id}">
						<option selected = "selected" value="${desconto.id}">${desconto.description}</option>
				    </c:when>
    				<c:otherwise>
						<option value="${desconto.id}">${desconto.description}</option>
				    </c:otherwise>
					</c:choose>
			</c:forEach> 
		</select>
   </div>
   <br>
	<div class="button" align="center">
		<a href="../../" style="text-decoration:none;"><input type="button" value="Back" align="left"></a>
		<input type="submit" value="Create Client" align="right">
	</div>

</form>
<c:if test="${model.hasMessages}">
	<h3 align="center">Result:</h3>
	<c:forEach var="mensagem" items="${model.messages}">
	${mensagem}
	</c:forEach>
</c:if>
</body>
</html>