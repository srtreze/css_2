<%--
  Created by IntelliJ IDEA.
  User: Telma
  Date: 16/12/16
  Time: 15:37
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  No scriptlets!!!
See http://download.oracle.com/javaee/5/tutorial/doc/bnakc.html
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="model" class="presentation.web.model.ReceiveOrderModel" scope="request"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="/resources/app.css">
    <title>SaleSys: order delivery</title>
</head>
<body>
<h2 align="center">Register a delivery of an order:</h2>
<c:if test="${model.hasMessages == false}">
	<form action="verEncomendaPorFornecedor" method="post" align="center">
		<div class="mandatory_field">
        	<label for="providerCode">Provider Code:</label>
        	<input type="text" name="providerCode" value="${model.providerCode}" autofocus/>
    	</div>
    	<br>
    	<div class="button" align="center">
        	<a href="../../" style="text-decoration:none;"><input type="button" value="Back" align="left"></a>
			<input type="submit" value="Show Orders of Provider">
		</div>
	</form>
</c:if>
<c:if test="${model.hasMessages}">
    <h3 align="center">Result:</h3>
    <c:choose>
		<c:when test="${model.messages[0]=='orders'}">
			<div class="button" align="center">
			    <c:forEach var="mensagem" items="${model.messages}">
					<c:if test="${mensagem != 'orders'}">
						${mensagem}
					</c:if>
		   		</c:forEach>
			</div>
	   		<form action="receberAEncomenda" method="post" align="center">
	 			<div class="mandatory_field">
	        		<label for="id">Order ID:</label>
	        		<input type="text" name="id" size="30" value="${model.id}" autofocus/>
	    		</div>
	   			<div class="mandatory_field">
	        		<label for="qty">Received Quantity:</label>
	        		<input type="text" name="qty" value="${model.qty}"/>
	    		</div>
	    		<br>
	    		<div class="button" align="center">
		        	<a href="../../" style="text-decoration:none;"><input type="button" value="Menu" align="left"></a>
	        		<input type="submit" value="Register delivery">
	    		</div>
			</form>	
		</c:when>
		<c:otherwise>
		    <c:forEach var="mensagem" items="${model.messages}">
				${mensagem}
	   		</c:forEach>
			<div class="button" align="center">
				<a href="../../" style="text-decoration:none;"><input type="button" value="Menu"></a>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>
</body>
</html>
