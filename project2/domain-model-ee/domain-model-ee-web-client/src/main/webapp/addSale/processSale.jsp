<%--
  Created by IntelliJ IDEA.
  User: Telma
  Date: 16/12/16
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  No scriptlets!!!
See http://download.oracle.com/javaee/5/tutorial/doc/bnakc.html
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="model" class="presentation.web.model.ProcessSaleModel" scope="request"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="/resources/app.css">
    <title>SaleSys: Process Sale</title>
</head>
<body>
<h2 align="center">Create Sale</h2>
<c:if test="${model.hasMessages == false}">
    <form action="criarVenda" method="post" align="center">
        <div class="mandatory_field">
            <label for="customer">Customer VAT Code:</label>
            <input type="text" name="customer" value="${model.customer}" autofocus/>
        </div>
        <br>
        <div class="mandatory_field">
            <label for="datee">Today's Date (dd-mm-yyyy):</label>
            <input type="text" name="datee" value="${model.datee}"/>
        </div>
        <br>
        <div class="button" align="center">
            <a href="../../" style="text-decoration:none;"><input type="button" value="Back" align="left"></a>
            <input type="submit" value="Add Sale" align="right">
        </div>
    </form>
</c:if>
<c:if test="${model.hasMessages}">
    <h3 align="center">Result:</h3>
    <c:choose>
        <c:when test="${model.messages[0].startsWith('id')}">
            ${model.messages[1]}
            ${model.messages[0]}
            <c:set var="sale" value="${fn:split(model.messages[0], ' ')}"></c:set>
            ${sale[1]}
            <c:set var="saleId" value="${sale[1]}"></c:set>

            <form action="adicionarProdutoVenda" method="post" align="center">
                <div class="mandatory_field" style="visibility: hidden;">
                    <label for="saleId" style="visibility: hidden;">Sale Id:</label>
                    <input name = "saleId" type="hidden" value="${saleId}">
                </div>
                <br>
                <div class="mandatory_field">
                    <label for="productCode">Product Code:</label>
                    <input type="text" name="productCode" value="${model.productCode}" autofocus/>
                </div>
                <div class="mandatory_field">
                    <label for="qty">Product Quantity:</label>
                    <input type="text" name="qty" value="${model.qty}" autofocus/>
                </div>
                <br>
                <div class="button" align="center">
                    <a href="../../" style="text-decoration:none;"><input type="button" value="Menu" align="left"></a>
                    <input type="submit" value="Add Product To Sale" align="right">
                </div>
            </form>
            <br>
            <div align="center" style="display:inline">
	            <form action="fecharVenda" method="post">
	            	<input name = "saleId" type="hidden" value="${saleId}">
	                <input type="submit" value="Close Sale" align="right">
	            </form>          
	            <form action="cancelarVenda" method="post">
	           		<input name = "saleId" type="hidden" value="${saleId}">
	                <input type="submit" value="Cancel Sale" align="right">
	            </form>
            </div>

        </c:when>
        <c:otherwise>
            <c:forEach var="mensagem" items="${model.messages}">
                ${mensagem}
            </c:forEach>
            <div class="button" align="center">
                <a href="../../" style="text-decoration:none;"><input type="button" value="Menu"></a>
            </div>
        </c:otherwise>
    </c:choose>
</c:if>

</body>
</html>