<%--
  Created by IntelliJ IDEA.
  User: Telma
  Date: 16/12/16
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  No scriptlets!!!
See http://download.oracle.com/javaee/5/tutorial/doc/bnakc.html
-->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="model" class="presentation.web.model.NewOrderModel" scope="request"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" type="text/css" href="/resources/app.css">
    <title>SaleSys: Create Order</title>
</head>
<body>
<h2 align="center">Create Order</h2>
<c:if test="${model.hasMessages == false}">
	<form action="mostraProvider" method="post" align="center">
     	<div class="mandatory_field">
        	<label for="providerCode">Provider Code:</label>
    	    <input type="text" name="providerCode" value="${model.providerCode}" autofocus/>
	    </div>
	    <br>
    	<div class="button" align="center">
        	<a href="../../" style="text-decoration:none;"><input type="button" value="Back" align="left"></a>
    	    <input type="submit" value="Next" align="right">
	    </div>
	</form>
</c:if>
<c:if test="${model.hasMessages}">
    <h3 align="center">Result:</h3>
    <c:choose>
		<c:when test="${model.messages[0]=='provider'}">
		    ${model.messages[1]}
			<form action="mostraProduto" method="post" align="center">
	    		<div class="mandatory_field">
    	    		<label for="productCode">Product Code:</label>
        			<input type="text" name="productCode" value="${model.productCode}" autofocus/>
			    </div>
			    <br>
	    		<div class="button" align="center">
        			<a href="../../" style="text-decoration:none;"><input type="button" value="Menu" align="left"></a>
    		    	<input type="submit" value="Next" align="right">
	    		</div>
			</form>	
		</c:when>
		<c:when test="${model.messages[0]=='product'}">
		    ${model.messages[1]}
			<form action="criarEncomenda" method="post" align="center">
			    <div class="mandatory_field">
			        <label for="providerCode">Provider Code:</label>
			        <input type="text" name="providerCode" size="68" value="${model.providerCode}" autofocus/>
			    </div>
			    <div class="mandatory_field">
			        <label for="productCode">Product Code:</label>
			        <input type="text" name="productCode" size="69" value="${model.productCode}"/>
			    </div>
			    <div class="mandatory_field">
			        <label for="qty">Quantity to Order:</label>
			        <input type="text" name="qty" size="65" value="${model.qty}"/>
			    </div>
			    <div class="mandatory_field">
			        <label for="expectedDate">Expected Date to Receive the Order (Formato dd-mm-yyyy):</label>
			        <input type="text" name="expectedDate" value="${model.expectedDate}"/> <br/>
			    </div>
			    <br>
			    <div class="button" align="center">
			        <a href="../../" style="text-decoration:none;"><input type="button" value="Menu" align="left"></a>
			        <input type="submit" value="Create Order" align="right">
			    </div>
			</form>	
		</c:when>
		<c:otherwise>
			<c:forEach var="mensagem" items="${model.messages}">
        	${mensagem}
        	</c:forEach>
			<div class="button" align="center">
				<a href="../../" style="text-decoration:none;"><input type="button" value="Menu"></a>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>

</body>
</html>