package presentation.web.model;

import java.util.Date;
import java.util.LinkedList;

import facade.dto.ProductDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IAddCustomerHandlerRemote;
import facade.handlers.IAddOrderHandlerRemote;
import facade.interfaces.IDiscount;
import facade.interfaces.IProvider;

/**
 * Helper class to assist in the response of nova encomenda.
 * This class is the response information expert.
 *
 */
public class NewOrderModel extends Model {


    private String productCode;
    private String id;
    private String providerCode;
    private String orderDate;
    private String qty;
    private String expectedDate;
    private String hasPrev;
    private IAddOrderHandlerRemote addOrderHandler;

    public void setAddOrderHandler(IAddOrderHandlerRemote addOrderHandler) {
        this.addOrderHandler = addOrderHandler;
    }

    public void setproductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getproductCode() {
        return productCode;
    }

    public String getid() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getproviderCode() {
        return providerCode;
    }

    public void setproviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public void setorderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getorderDate() {
        return orderDate;
    }

    public void setqty(String qty) {
        this.qty = qty;
    }

    public String getqty() {
        return qty;
    }

    public void setexpectedDate(String expectedDate) {
        this.expectedDate = expectedDate;
    }

    public String getexpectedDate() {
        return expectedDate;
    }

    public void sethasPrev(String hasPrev) {
        this.hasPrev = hasPrev;
    }

    public String gethasPrev() {
        return hasPrev;
    }

    public void clearfields() {
        productCode = id = providerCode = orderDate = qty = expectedDate = "";
        hasPrev = "false";
    }

    //TODO
    public IProvider getprovider() throws ApplicationException {
        try {
            return (IProvider) addOrderHandler.getProvider(Integer.parseInt(providerCode));
        } catch (ApplicationException e) {
            throw new ApplicationException("error to get provider", e);
        }
    }

    //TODO
    public ProductDTO getproduct() throws ApplicationException {
        try {
            return addOrderHandler.getProduct(Integer.parseInt(productCode));
        } catch (ApplicationException e) {
            throw new ApplicationException("error to get provider products", e);
        }
    }
}
