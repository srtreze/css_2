package presentation.web.model;

import facade.exceptions.ApplicationException;

import facade.handlers.IReceiveOrderHandlerRemote;


/**
 * Helper class to assist in the response of nova encomenda.
 * This class is the response information expert.
 *
 */
public class ReceiveOrderModel extends Model {


    private String providerCode;
    private String productCode;
    private String id;
    private String qty;
    private IReceiveOrderHandlerRemote receiveOrderHandler;

    public void setReceiveOrderHandler(IReceiveOrderHandlerRemote receiveOrderHandler) {
        this.receiveOrderHandler = receiveOrderHandler;
    }

    public void setproviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    public String getproviderCode() {
        return providerCode;
    }

    public void setproductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getproductCode() {
        return productCode;
    }

    public String getid() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public void setqty(String qty) {
        this.qty = qty;
    }

    public String getqty() {
        return qty;
    }

    public void clearfields() {
        productCode = id = qty = "";
    }

    //TODO
    public String processOrder(){
        try {
            receiveOrderHandler.processOrder(Integer.parseInt(id), Double.parseDouble(qty));
            return "order process succeeded";
        } catch (ApplicationException e) {
            return "error in processing order";
        }
    }
    
    //TODO
    public String receiveOrder() {
        try {
            receiveOrderHandler.receiveOrder(Integer.parseInt(providerCode));
            return "order received succeeded";
        } catch (ApplicationException e) {
            return "error in receiving order";
        }
    }
}