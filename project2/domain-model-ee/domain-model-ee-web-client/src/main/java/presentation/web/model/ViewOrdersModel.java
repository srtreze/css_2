package presentation.web.model;

import facade.dto.OrderDTO;
import facade.exceptions.ApplicationException;

import facade.handlers.IViewOrderHandlerRemote;
import facade.interfaces.IOrder;

import java.awt.peer.SystemTrayPeer;
import java.util.Iterator;
import java.util.LinkedList;


/**
 * Helper class to assist in the response of ver encomendas.
 * This class is the response information expert.
 *
 */
public class ViewOrdersModel extends Model {


    private String productCode;
    private IViewOrderHandlerRemote viewOrderHandler;

    public void setViewOrderHandler(IViewOrderHandlerRemote viewOrderHandler) {
        this.viewOrderHandler = viewOrderHandler;
    }

    public void setproductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getproductCode() {
        return productCode;
    }


    public void clearFields() {
        productCode = "";
    }


    //TODO
    public Iterable<? extends IOrder> viewOrders(){
        try {
            Iterable<OrderDTO> orders = viewOrderHandler.getPendentProd(Integer.parseInt(productCode));
            LinkedList<IOrder> iorders = new LinkedList<>();
            for (OrderDTO order: orders
                 ) {
                iorders.add((IOrder)order);
            }
            return iorders;
        } catch (ApplicationException e) {
            return new LinkedList<IOrder>();
        }
    }

}