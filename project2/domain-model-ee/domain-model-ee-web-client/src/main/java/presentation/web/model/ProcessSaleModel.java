package presentation.web.model;

import java.util.Date;
import java.util.LinkedList;

import facade.dto.ProductDTO;
import facade.dto.SaleProductDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IAddCustomerHandlerRemote;
import facade.handlers.IAddOrderHandlerRemote;
import facade.handlers.IProcessSaleHandlerRemote;
import facade.interfaces.IDiscount;
import facade.interfaces.IProvider;

/**
 * Helper class to assist in the response of process sale.
 * This class is the response information expert.
 *
 */
public class ProcessSaleModel extends Model {


    private String saleId;
    private String customer;
    private String datee;
    private String productCode;
    private String qty;


    private IProcessSaleHandlerRemote processSaleHandler;

    public void setProcessSaleHandler(IProcessSaleHandlerRemote processSaleHandler) {
        this.processSaleHandler = processSaleHandler;
    }

    public void setcustomer(String customer) {
        this.customer = customer;
    }

    public String getcustomer() {
        return customer;
    }

    public String getsaleId() {
        return saleId;
    }

    public void setsaleId(String saleId) {
        this.saleId = saleId;
    }

    public void setdatee(String datee) {
        this.datee = datee;
    }

    public String getdatee() {
        return datee;
    }

    public void setqty(String qty) {
        this.qty = qty;
    }

    public String getqty() {
        return qty;
    }

    public void setproductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getproductCode() {
        return productCode;
    }


    public void clearfields() {
        customer = saleId = datee = productCode = qty ="";
    }



    //TODO
    public void addProductToSale() throws ApplicationException {
        try {
            processSaleHandler.addProductToSale(Integer.parseInt(saleId), Integer.parseInt(productCode), Double.parseDouble(qty));
        } catch (ApplicationException e) {
            throw new ApplicationException("error to add product to sale ", e);
        }
    }

    //TODO
    public Iterable<SaleProductDTO> getproducts() throws ApplicationException {
        try {
            return processSaleHandler.getProducts(Integer.parseInt(saleId));
        } catch (ApplicationException e) {
            throw new ApplicationException("error to get sale products", e);
        }
    }

    //TODO
    public void closeSale() throws ApplicationException{
        try {
            processSaleHandler.closeSale(Integer.parseInt(saleId));
        } catch (ApplicationException e) {
            throw new ApplicationException("error to close sale ", e);
        }
    }

    //TODO
    public void cancelSale() throws ApplicationException{
        try {
            processSaleHandler.cancelSale(Integer.parseInt(saleId));
        } catch (ApplicationException e) {
            throw new ApplicationException("error to cancel sale ", e);
        }
    }
}

