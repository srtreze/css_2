package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IReceiveOrderHandlerRemote;
import facade.exceptions.ApplicationException;
import presentation.web.model.ReceiveOrderModel;


/**
 * Handles the receive encomenda event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response.
 *
 *
 */
@Stateless
public class ReceiveCreateOrderAction extends Action {

    @EJB private IReceiveOrderHandlerRemote receiveOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ReceiveOrderModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
                receiveOrderHandler.processOrder(intValue(model.getid()), intValue(model.getqty()));
                model.clearfields();
//                model.addMessage("done");
                model.addMessage("<div align='center'>Order successfully received.</div>");
            } catch (ApplicationException e) {
                model.addMessage("<div align='center'>Error receiving order: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating order data</div>");

        request.getRequestDispatcher("/receiveOrder/receiveOrder.jsp").forward(request, response);
    }


    private boolean validInput(ReceiveOrderModel model) {


        // check if qty is filled and a valid number
        boolean result = isFilled(model, model.getqty(), "<div align='center'>qty must be filled</div>")
        		&& isInt(model, model.getqty(), "<div align='center'>qty with invalid characters</div>");

        // check if qty is filled and a valid number
        result &= isFilled(model, model.getid(), "<div align='center'>id must be filled</div>")
                && isInt(model, model.getid(), "<div align='center'>id with invalid characters</div>");

        return result;
    }

    //TODO
    private ReceiveOrderModel createModel(HttpServletRequest request) {
        // Create the object model
        ReceiveOrderModel model = new ReceiveOrderModel();
        model.setReceiveOrderHandler(receiveOrderHandler);

        // fill it with data from the request
        model.setid(request.getParameter("id"));
        model.setqty(request.getParameter("qty"));

        return model;
    }
}
