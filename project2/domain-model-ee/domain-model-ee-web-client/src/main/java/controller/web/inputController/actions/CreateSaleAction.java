package controller.web.inputController.actions;

import java.io.IOException;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.dto.SaleDTO;
import facade.handlers.IProcessSaleHandlerRemote;

import presentation.web.model.ProcessSaleModel;

/**
 * Created by Telma on 19/12/16.
 */
/**
 * Handles the criar encomenda event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response.
 *
 *
 */
@Stateless
public class CreateSaleAction extends Action {

    @EJB private IProcessSaleHandlerRemote processSaleHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProcessSaleModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
                SaleDTO s = processSaleHandler.newSale(dateValue(model.getdatee()), intValue(model.getcustomer()));
                model.clearfields();
                model.addMessage("id " + s.id);
                model.addMessage("<div align='center'>Sale successfully added.</div>");
            } catch (Exception e) {
                model.addMessage("<div align='center'>Error adding sale: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating sale data</div>");

        request.getRequestDispatcher("/addSale/processSale.jsp").forward(request, response);
    }


    private boolean validInput(ProcessSaleModel model) {

        // check if provider code is filled and a valid number
        boolean result = isFilled(model, model.getcustomer(), "<div align='center'>customer vat number must be filled</div>")
                && isInt(model, model.getcustomer(), "<div align='center'>customer vat number with invalid characters</div>");

        // check if expected date is filled
        result &= isFilled(model, model.getdatee(), "<div align='center'>date must be filled.</div>");

        return result;
    }

    private ProcessSaleModel createModel(HttpServletRequest request){
        // Create the object model
        ProcessSaleModel model = new ProcessSaleModel();
        model.setProcessSaleHandler(processSaleHandler);

        // fill it with data from the request
        model.setcustomer(request.getParameter("customer"));
        model.setdatee(request.getParameter("datee"));

        return model;
    }
}
