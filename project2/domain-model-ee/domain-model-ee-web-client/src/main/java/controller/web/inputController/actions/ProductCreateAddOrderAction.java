package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.dto.ProductDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IAddOrderHandlerRemote;
import presentation.web.model.NewOrderModel;

@Stateless
public class ProductCreateAddOrderAction extends Action {

    @EJB private IAddOrderHandlerRemote addOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        NewOrderModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
                ProductDTO product = addOrderHandler.getProduct(intValue(model.getproductCode()));
                model.clearfields();
                model.addMessage("product");
                model.addMessage(productToString(product));
            } catch (ApplicationException e) {
                model.addMessage("<div align='center'>Error getting product: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating order data</div>");

        request.getRequestDispatcher("/addOrder/newOrder.jsp").forward(request, response);
    }


    private boolean validInput(NewOrderModel model) {

        // check if product code is filled and a valid number
        boolean result = isFilled(model, model.getproductCode(), "<div align='center'>productCode code number must be filled</div>")
                && isInt(model, model.getproductCode(), "<div align='center'>productCode code number with invalid characters</div>");

        return result;
    }

    private String productToString(ProductDTO product){
        String productst ="<div align='center'><h3>Product : " + product.description + "</h3><br>" + "<strong>Product code: " + product.prodCode + "</strong>" + "<br>"+"Face Value: " + product.faceValue + ";" + "<br>"+"Quantity: " + product.qty + ";" + "<br>"+"Pendent Quantity: " + product.pendentQty + ";" + "</div><br>" + "<br>" ;
        return productst;
    }

    //TODO
    private NewOrderModel createModel(HttpServletRequest request) {
        // Create the object model
        NewOrderModel model = new NewOrderModel();
        model.setAddOrderHandler(addOrderHandler);

        // fill it with data from the request
        model.setproductCode(request.getParameter("productCode"));

        return model;
    }
}
