package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.dto.ProductDTO;
import facade.dto.ProviderDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IAddOrderHandlerRemote;
import presentation.web.model.NewOrderModel;

@Stateless
public class ProviderCreateAddOrderAction extends Action {

    @EJB private IAddOrderHandlerRemote addOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        NewOrderModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
            	ProviderDTO prov = addOrderHandler.getProvider(intValue(model.getproviderCode()));
                model.clearfields();
                model.addMessage("provider");
                model.addMessage(providerToString(prov));
            } catch (ApplicationException e) {
                model.addMessage("<div align='center'>Error getting provider: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating order data</div>");

        request.getRequestDispatcher("/addOrder/newOrder.jsp").forward(request, response);
    }


    private boolean validInput(NewOrderModel model) {

        // check if product code is filled and a valid number
        boolean result = isFilled(model, model.getproviderCode(), "<div align='center'>productCode code number must be filled</div>")
                && isInt(model, model.getproviderCode(), "<div align='center'>productCode code number with invalid characters</div>");

        return result;
    }

    private String providerToString(ProviderDTO provider){
        String prov ="<div align='center'><strong>Provider : " + provider.name + "</strong><br>" + "<strong align='center'>Provider code: " + provider.code + "</strong></div>" + "<br>";
        return prov;
    }

    //TODO
    private NewOrderModel createModel(HttpServletRequest request) {
        // Create the object model
        NewOrderModel model = new NewOrderModel();
        model.setAddOrderHandler(addOrderHandler);

        // fill it with data from the request
        model.setproviderCode(request.getParameter("providerCode"));

        return model;
    }
}
