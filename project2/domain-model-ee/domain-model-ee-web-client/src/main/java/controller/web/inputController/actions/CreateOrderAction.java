package controller.web.inputController.actions;

import java.io.IOException;
import java.sql.Date;
import java.util.DoubleSummaryStatistics;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IAddOrderHandlerRemote;
import presentation.web.model.NewOrderModel;
import facade.exceptions.ApplicationException;


/**
 * Handles the criar encomenda event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response.
 *
 *
 */
@Stateless
public class CreateOrderAction extends Action {

    @EJB private IAddOrderHandlerRemote addOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        NewOrderModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
            	
                addOrderHandler.addOrder(intValue(model.getproductCode()),
                        dateValue(model.getexpectedDate()), Double.parseDouble(model.getqty()), intValue(model.getproviderCode()));
                model.clearfields();
                model.addMessage("<div align='center'>Order successfully added.</div>");
            } catch (Exception e) {
                model.addMessage("<div align='center'>Error adding order: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating order data</div>");

        request.getRequestDispatcher("/addOrder/newOrder.jsp").forward(request, response);
    }


    private boolean validInput(NewOrderModel model) {

        // check if provider code is filled and a valid number
        boolean result = isFilled(model, model.getproviderCode(), "<div align='center'>provider code number must be filled</div>")
                && isInt(model, model.getproviderCode(), "<div align='center'>provider code number with invalid characters</div>");

        // check if product code is filled and a valid number
        result &= isFilled(model, model.getproductCode(), "<div align='center'>product code number must be filled</div>")
                && isInt(model, model.getproductCode(), "<div align='center'>product code number with invalid characters</div>");

        // check if qty is filled and a valid number
        result &= isFilled(model, model.getqty(), "<div align='center'>qty must be filled</div>")
                && isInt(model, model.getqty(), "<div align='center'>qty with invalid characters</div>");

        // check if expected date is filled
        result &= isFilled(model, model.getexpectedDate(), "<div align='center'>expected date must be filled.</div>");

        return result;
    }

    private NewOrderModel createModel(HttpServletRequest request) {
        // Create the object model
        NewOrderModel model = new NewOrderModel();
        model.setAddOrderHandler(addOrderHandler);

        // fill it with data from the request
        model.setproviderCode(request.getParameter("providerCode"));
        model.setproductCode(request.getParameter("productCode"));
        model.setqty(request.getParameter("qty"));
        model.setexpectedDate(request.getParameter("expectedDate"));

        return model;
    }
}
