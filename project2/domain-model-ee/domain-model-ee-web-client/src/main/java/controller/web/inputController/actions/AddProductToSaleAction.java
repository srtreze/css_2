package controller.web.inputController.actions;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.atomic.DoubleAccumulator;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IProcessSaleHandlerRemote;

import presentation.web.model.ProcessSaleModel;

/**
 * Created by Telma on 19/12/16.
 */
/**
 * Handles the criar encomenda event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response.
 *
 *
 */
@Stateless
public class AddProductToSaleAction extends Action {

    @EJB private IProcessSaleHandlerRemote processSaleHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProcessSaleModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
                processSaleHandler.addProductToSale(intValue(model.getsaleId()), intValue(model.getproductCode()), Double.parseDouble(model.getqty()));
                model.addMessage("id " + model.getsaleId());
                model.clearfields();
                model.addMessage("<div align='center'>Product successfully added to sale.</div>");
            } catch (Exception e) {
                model.addMessage("<div align='center'>Error adding product: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating sale data</div>");

        request.getRequestDispatcher("/addSale/processSale.jsp").forward(request, response);
    }


    private boolean validInput(ProcessSaleModel model) {

        // check if provider code is filled and a valid number
        boolean result = isFilled(model, model.getsaleId(), "<div align='center'>sale  number must be filled</div>")
                && isInt(model, model.getsaleId(), "<div align='center'>sale number with invalid characters</div>");

        // check if provider code is filled and a valid number
        result &= isFilled(model, model.getproductCode(), "<div align='center'>product number must be filled</div>")
                && isInt(model, model.getproductCode(), "<div align='center'>product number with invalid characters</div>");

        // check if expected date is filled
        result &= isFilled(model, model.getqty(), "<div align='center'>quantity must be filled.</div>");

        return result;
    }

    private ProcessSaleModel createModel(HttpServletRequest request) {
        // Create the object model
        ProcessSaleModel model = new ProcessSaleModel();
        model.setProcessSaleHandler(processSaleHandler);

        // fill it with data from the request
        model.setsaleId(request.getParameter("saleId"));
        model.setproductCode(request.getParameter("productCode"));
        model.setqty(request.getParameter("qty"));

        return model;
    }
}
