package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.dto.OrderDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IViewOrderHandlerRemote;
import presentation.web.model.ViewOrdersModel;


/**
 * Handles the view encomenda event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response.
 *
 *
 */
@Stateless
public class ViewCreateOrdersAction extends Action {

    @EJB private IViewOrderHandlerRemote viewOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ViewOrdersModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
                Iterable<OrderDTO> orders = viewOrderHandler.getPendentProd(intValue(model.getproductCode()));
                model.clearFields();
                model.addMessage(ordersToString(orders));
            } catch (ApplicationException e) {
                model.addMessage("Error view order: " + e.getMessage());
            }
        } else
            model.addMessage("Error validating order data");

        request.getRequestDispatcher("/viewOrder/viewOrder.jsp").forward(request, response);
    }


    private boolean validInput(ViewOrdersModel model) {


        // check if product code is filled and a valid number
        boolean result = isFilled(model, model.getproductCode(), "product code number must be filled")
                && isInt(model, model.getproductCode(), "product code number with invalid characters");

        return result;
    }


    private String ordersToString(Iterable<OrderDTO> orders){
        String orderst ="";
        for (OrderDTO order: orders) {
            orderst += "<strong>Order id: " + order.id + "</strong>" + "<br>"+"&nbsp &nbsp &nbsp Product: " + order.productCode + ";" + "<br>"+"&nbsp &nbsp &nbsp Quantity ordered: " + order.qty + ";" + "<br>"+"&nbsp &nbsp &nbsp Date of order: " + order.orderDate + ";" + "<br>"+"&nbsp &nbsp &nbsp Date expected to receive: " + order.expectedDate + ";" + "<br>"+"&nbsp &nbsp &nbsp Provider name: " + order.provDesig + ";" + "<br>"+"&nbsp &nbsp &nbsp Previous order associated? " + order.hasPrev + "<br>" + "<br>" ;
        }
        return orderst;
    }

    //TODO
    private ViewOrdersModel createModel(HttpServletRequest request) {
        // Create the object model
        ViewOrdersModel model = new ViewOrdersModel();
        model.setViewOrderHandler(viewOrderHandler);

        // fill it with data from the request
        model.setproductCode(request.getParameter("productCode"));

        return model;
    }
}
