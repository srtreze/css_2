package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IViewOrderHandlerRemote;
import presentation.web.model.ViewOrdersModel;


/**
 * Handles the view encomenda event
 *
 *
 */
@Stateless
public class ViewOrdersAction extends Action {

    @EJB private IViewOrderHandlerRemote viewOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ViewOrdersModel model = new ViewOrdersModel();
        model.setViewOrderHandler(viewOrderHandler);
        request.setAttribute("model", model);
        request.getRequestDispatcher("/viewOrder/viewOrder.jsp").forward(request, response);
    }

}
