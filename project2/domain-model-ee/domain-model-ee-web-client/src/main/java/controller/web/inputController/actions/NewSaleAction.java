package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IProcessSaleHandlerRemote;
import presentation.web.model.ProcessSaleModel;

/**
 * Created by Telma on 19/12/16.
 */

/**
 * Handles the nova venda event
 *
 *
 */
@Stateless
public class NewSaleAction extends Action {

    @EJB private IProcessSaleHandlerRemote processSaleHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProcessSaleModel model = new ProcessSaleModel();
        model.setProcessSaleHandler(processSaleHandler);
        request.setAttribute("model", model);
        request.getRequestDispatcher("/addSale/processSale.jsp").forward(request, response);
    }

}
