package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IProcessSaleHandlerRemote;

import presentation.web.model.ProcessSaleModel;

/**
 * Created by Telma on 19/12/16.
 */
/**
 * Handles the criar encomenda event.
 * If the request is valid, it dispatches it to the domain model (the application's business logic)
 * Notice as well the use of an helper class to assist in the jsp response.
 *
 *
 */
@Stateless
public class CloseSaleAction extends Action {

    @EJB private IProcessSaleHandlerRemote processSaleHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProcessSaleModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
                double[] td = processSaleHandler.closeSale(intValue(model.getsaleId()));
                model.clearfields();
                model.addMessage("<div align='center'> Total : "+ td[0] + "; Discount : "+ td[1] +"</div>");
                model.addMessage("<div align='center'>Sale successfully closed.</div>");
            } catch (Exception e) {
                model.addMessage("<div align='center'>Error closing sale: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating sale data</div>");

        request.getRequestDispatcher("/addSale/processSale.jsp").forward(request, response);
    }


    private boolean validInput(ProcessSaleModel model) {

        // check if provider code is filled and a valid number
        boolean result = isFilled(model, model.getsaleId(), "<div align='center'>sale id number must be filled</div>")
                && isInt(model, model.getsaleId(), "<div align='center'>sale id number with invalid characters</div>");

        return result;
    }

    private ProcessSaleModel createModel(HttpServletRequest request) {
        // Create the object model
        ProcessSaleModel model = new ProcessSaleModel();
        model.setProcessSaleHandler(processSaleHandler);

        // fill it with data from the request
        model.setsaleId(request.getParameter("saleId"));

        return model;
    }
}
