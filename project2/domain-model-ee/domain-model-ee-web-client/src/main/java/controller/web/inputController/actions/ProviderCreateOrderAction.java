package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.dto.OrderDTO;
import facade.exceptions.ApplicationException;
import facade.handlers.IReceiveOrderHandlerRemote;
import presentation.web.model.ReceiveOrderModel;

@Stateless
public class ProviderCreateOrderAction extends Action {

    @EJB private IReceiveOrderHandlerRemote receiveOrderHandler;

    @Override
	public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        ReceiveOrderModel model = createModel(request);
        request.setAttribute("model", model);

        if (validInput(model)) {
            try {
            	Iterable<OrderDTO> orders = receiveOrderHandler.receiveOrder(intValue(model.getproviderCode()));
                model.clearfields();
                model.addMessage("orders");
                model.addMessage(ordersToString(orders));
            } catch (ApplicationException e) {
                model.addMessage("<div align='center'>Error getting provider: " + e.getMessage()+"</div>");
            }
        } else
            model.addMessage("<div align='center'>Error validating order data</div>");

        request.getRequestDispatcher("/receiveOrder/receiveOrder.jsp").forward(request, response);
    }


    private boolean validInput(ReceiveOrderModel model) {


         // check if product code is filled and a valid number
        boolean result = isFilled(model, model.getproviderCode(), "<div align='center'>providerCode code number must be filled</div>")
                && isInt(model, model.getproviderCode(), "<div align='center'>providerCode code number with invalid characters</div>");
        
        return result;
    }

    private String ordersToString(Iterable<OrderDTO> orders){
        String orderst ="";
        for (OrderDTO order: orders) {
        	if(orderst == ""){
        		orderst += "<h3>Provider name: " + order.provDesig + "</h3>";
        	}
            orderst += "&nbsp &nbsp &nbsp <strong>Order id: " + order.id + "</strong>" + "<br>"+"&nbsp &nbsp &nbsp Product: " + order.productCode + ";" + "<br>"+"&nbsp &nbsp &nbsp Quantity ordered: " + order.qty + ";" + "<br>"+"&nbsp &nbsp &nbsp Date of order: " + order.orderDate + ";" + "<br>"+"&nbsp &nbsp &nbsp Date expected to receive: " + order.expectedDate + ";" + "<br>"+"&nbsp &nbsp &nbsp Previous order associated? " + order.hasPrev + "<br>" + "<br>" ;

        }
        return orderst;
    }
 
    //TODO
    private ReceiveOrderModel createModel(HttpServletRequest request) {
        // Create the object model
        ReceiveOrderModel model = new ReceiveOrderModel();
        model.setReceiveOrderHandler(receiveOrderHandler);

        // fill it with data from the request
        model.setproviderCode(request.getParameter("providerCode"));

        return model;
    }
}
