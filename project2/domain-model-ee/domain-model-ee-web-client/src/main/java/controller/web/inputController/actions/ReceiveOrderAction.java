package controller.web.inputController.actions;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facade.handlers.IAddOrderHandlerRemote;
import facade.handlers.IReceiveOrderHandlerRemote;
import presentation.web.model.NewOrderModel;
import presentation.web.model.ReceiveOrderModel;


/**
 * Handles the receive encomenda event
 *
 *
 */
@Stateless
public class ReceiveOrderAction extends Action {

    @EJB private IReceiveOrderHandlerRemote receiveOrderHandler;

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ReceiveOrderModel model = new ReceiveOrderModel();
        model.setReceiveOrderHandler(receiveOrderHandler);
        request.setAttribute("model", model);
        request.getRequestDispatcher("/receiveOrder/receiveOrder.jsp").forward(request, response);
    }

}
